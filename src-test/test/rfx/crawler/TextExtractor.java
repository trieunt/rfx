package test.rfx.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import de.l3s.boilerpipe.extractors.ArticleExtractor;

public class TextExtractor {

    static String sampleUrl = "http://thethao.vnexpress.net/tin-tuc/giai-ngoai-hang-anh/chelsea-lo-co-hoi-gia-tang-suc-ep-len-arsenal-va-man-city-2950223.html";

    static void extractArticle(String url) {
        try {

            Document doc = Jsoup.connect(url).get();
            String title = doc.select("title").text();
            String keywords = doc.select("meta[name=keywords]").attr("content");
            String bodyHtml = doc.select("body").html();

            String text = ArticleExtractor.INSTANCE.getText(bodyHtml);

            System.out.println("title: " + title);
            System.out.println("keywords: " + keywords);
            System.out.println("\n content: \n" + text);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }    

    public static void main(String[] args) {

    }
}
