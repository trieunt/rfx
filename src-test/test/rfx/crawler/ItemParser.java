/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.rfx.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import rfx.core.util.HttpClientUtil;

/**
 *
 * @author tantrieuf31
 */
public class ItemParser {

    public static void main(String[] args) {
        String href = "http://diadiemanuong.com/beta/tp-ho-chi-minh/quan-7/papas-chicken-phu-my-hung-2698.html";
        Document itemDoc = Jsoup.parse(HttpClientUtil.executeGet(href));
        System.out.println("title: " + itemDoc.title() + " url:" + href + " \n");
        String imageUrl = itemDoc.select("div[class=thumb]").select("img").get(0).outerHtml();
        System.out.println(imageUrl);
    }
}
