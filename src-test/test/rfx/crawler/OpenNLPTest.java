package test.rfx.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import de.l3s.boilerpipe.extractors.ArticleExtractor;

public class OpenNLPTest {

	static String extractArticle(String url) {
		try {

			Document doc = Jsoup.connect(url).get();
			String title = doc.select("title").text();
			String keywords = doc.select("meta[name=keywords]").attr("content");
			String bodyHtml = doc.select("body").html();

			String text = ArticleExtractor.INSTANCE.getText(bodyHtml);

			System.out.println("title: " + title);
			System.out.println("keywords: " + keywords);
			System.out.println("\n content: \n" + text);
			return text;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public static void main(String[] args) {
		// http://opennlp.sourceforge.net/models-1.5/
		// FileIOUtil.downloadFile(url, filename)

		try {
			String url = "http://giaitri.vnexpress.net/tin-tuc/gioi-sao/trong-nuoc/diem-huong-hut-hang-khi-bi-huy-vai-dien-2968778.html";
			System.out.println(extractArticle(url));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
// http://kewi.unomaha.edu/resources/OpenNLPTutorial.pdf
