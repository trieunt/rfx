package test.rfx.lucene;

import java.io.IOException;
import java.util.List;

import rfx.core.model.crawler.Item;
import rfx.core.search.LuceneSearchManager;

public class UnitTestSearchItem {

	public static void main(String[] args) throws IOException {
		Item item1 = new Item("http://www.danhsachviet.vn/diadiem/chitiet/12906/quan-26", "QUÁN 26");
		item1.setTotalSocialScore(20);
		item1.setContextLocation("77/6 Trần Xuân Soạn - Phường Tân Thuận Tây , Quận 7, TP. Hồ Chí Minh");
		
		Item item2 = new Item("http://www.danhsachviet.vn/diadiem/chitiet/12758/tatu-beer-club", "TATU BEER CLUB");
		item2.setTotalSocialScore(10);
		item2.setContextLocation("274 Nguyễn Văn Linh, Phường Bình Thuận, Quận 7, TP. Hồ Chí Minh");
		
		Item item3 = new Item("http://www.danhsachviet.vn/diadiem/chitiet/12690/massage-sao-mai-spa", "MASSAGE SAO MAI + SPA");
		item3.setTotalSocialScore(30);
		item3.setContextLocation("Số 06 Nguyễn Thị Thập, Phường Bình Thuận, Quận 7, TP. Hồ Chí Minh");
			
		
		LuceneSearchManager manager = new LuceneSearchManager(); 
		manager.indexAndStoreItems(item1,item2,item3);
		
		
		List<Item> items = manager.search("", "quận 7");
		for (Item item : items) {
			System.out.println(item.getName() + " " + item.getSocialMetric().getTotalSocialScore()); 
		}
	}
}
