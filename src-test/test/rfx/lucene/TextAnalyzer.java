package test.rfx.lucene;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.shingle.ShingleAnalyzerWrapper;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import rfx.core.util.io.FileUtils;

public class TextAnalyzer {
	public static void main(String[] args) {
		try {
			
			String text = FileUtils.readFileAsString("data/text/article-1.txt");
			// Parse the file into n-gram tokens
			KeywordAnalyzer simpleAnalyzer = new KeywordAnalyzer();
			ShingleAnalyzerWrapper shingleAnalyzer = new ShingleAnalyzerWrapper(simpleAnalyzer, 2, 4);

			TokenStream stream = shingleAnalyzer.tokenStream("contents", text);
			CharTermAttribute charTermAttribute = stream.getAttribute(CharTermAttribute.class);
			stream.reset();

			// Do something with the tokens
			//ArrayList<String> gram = new ArrayList<String>();
			while (stream.incrementToken()) {
				System.out.println(charTermAttribute.toString());
			}
			shingleAnalyzer.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
