package test.rfx.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import rfx.core.util.io.FileUtils;

public class ScanWorkerJarFile {

	public static void main(String[] args) throws IOException {
		File[] files = FileUtils.listFilesInForder("/deploy", new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {				
				return name.startsWith("worker-") && name.endsWith(".jar");
			}
		});
		for (File file : files) {
			System.out.println(file.getAbsolutePath());
		}
	}
}
