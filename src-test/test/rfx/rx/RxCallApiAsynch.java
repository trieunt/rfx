package test.rfx.rx;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import rfx.core.util.concurrent.HttpClientConcurrentCaller;
import rx.Observer;

public class RxCallApiAsynch {	
	
	public static void main(String[] args) {
		String url1 = "http://www.javacodegeeks.com/2013/07/java-futuretask-example-program.html";
		String url2 = "http://techblog.netflix.com/2013/02/rxjava-netflix-api.html";
		String url3 = "https://github.com/Netflix/RxJava/wiki/Creating-Observables";
		
		new HttpClientConcurrentCaller().call(url1, url2).subscribe(new Observer<String>() {
			@Override
			public void onCompleted() {
				System.out.println("Done!");				
			}
			@Override
			public void onError(Throwable arg0) {
				// TODO Auto-generated method stub				
			}
			@Override
			public void onNext(String html) {	
				Document doc = Jsoup.parse(html);
				System.out.println(doc.select("title").text());
			}
		});
		
		new HttpClientConcurrentCaller().call(url3).subscribe(new Observer<String>() {
			@Override
			public void onCompleted() {
				System.out.println("Done!");				
			}
			@Override
			public void onError(Throwable arg0) {
				// TODO Auto-generated method stub				
			}
			@Override
			public void onNext(String html) {	
				Document doc = Jsoup.parse(html);
				System.out.println(doc.select("title").text());
			}
		});
	}

}
