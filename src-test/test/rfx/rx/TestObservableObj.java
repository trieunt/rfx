package test.rfx.rx;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;


public class TestObservableObj {

	public static void hello(String... names) {
		Observable.from(names).subscribe(new Action1<String>() {
			@Override
			public void call(String s) {
				System.out.println("Hello " + s + "!");
			}
		});
	}

	public static Observable<String> mapStrings(String... names) {
		return Observable.from(names).map(new Func1<String, String>() {
			@Override
			public String call(String it) {
				// TODO Auto-generated method stub
				return it.toUpperCase();
			}
		});
	}
	
	static Action1<String> printData = new Action1<String>() {
		@Override
		public void call(String arg0) {
			System.out.println(arg0);
			if(arg0.contains("dd")){
				throw new IllegalAccessError("dd is bad arg!");
			}
		}
		
	};

	public static void main(String[] args) {
		String[] names = new String[]{"aa", "bb", "cc", "dd", "ee"};
		Observable<String> rs = Observable.from(names).map(new Func1<String, String>() {
			@Override
			public String call(String it) {
				if(it.contains("dd")){
					return "skip processing at dd";
				}
				return it.toUpperCase();
			}
		});
		rs.subscribe(new Action1<String>() {
			@Override
			public void call(String arg0) {
				System.out.println(arg0);				
			}			
		});
		
		
		
		
		if(rs != null){
			return;
		}
		
		hello("Tommy", "Peter", "Annie");
		
		System.out.println("\n mapStrings");
		
		System.out.println("\n flatMap");
		rs.flatMap(new Func1<String, Observable<String>>() {
			@Override
			public Observable<String> call(String arg0) {
				return Observable.just(arg0);
			}
		}).skip(1).take(3).subscribe(printData, new Action1<Throwable>() {			
			@Override
			public void call(Throwable arg0) {
				System.err.println(" on error: "+ arg0.getMessage());
			}
		}, new Action0() {			
			@Override
			public void call() {
				System.out.println("done");
				
			}
		});
		
		//http://www.aosabook.org/en/zeromq.html ?
		//a lock-free queue in pipe objects to pass messages between the user's threads and ØMQ's worker threads. 
		//There are two interesting aspects to how ØMQ uses the lock-free queue.
		Queue<String> stream = new ConcurrentLinkedQueue<String>();
		for (int i = 0; i < 18; i++) {
			stream.add("event:"+i);			
		}
		Observable<String> oStream = Observable.from(stream);
		
//		Observable.buffer(oStream, 5).subscribe(new Action1<List<String>>() {
//			
//			@Override
//			public void call(List<String> list) {
//				System.out.println("batch:" + list);	
//				try {
//					Thread.sleep(1000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		});
	}
}
