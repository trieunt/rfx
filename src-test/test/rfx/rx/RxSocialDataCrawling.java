package test.rfx.rx;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.Tuple;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.ClusterInfoConfigs;
import rfx.core.nosql.jedis.RedisCommand;
import rfx.core.util.StringUtil;

public class RxSocialDataCrawling {
	static ShardedJedisPool jedisPool = ClusterInfoConfigs.load().getClusterInfoRedis().getShardedJedisPool();

	public static void main(String[] args) {
		List<String> trendingKeywords = (new RedisCommand<List<String>>(jedisPool) {
            @Override
            public List<String> build() throws JedisException {	            	
            	Set<Tuple> set = jedis.zrevrangeWithScores("trending-keywords:2014-03-30-16-33", 0, -1);
            	List<String> list = new ArrayList<String>(set.size());
            	for (Tuple e : set) {
            		String str = e.getElement() + " " + e.getScore();
            		list.add(str);
				}
                return list;
            }
	    }).execute();
		System.out.println(StringUtil.joinFromList("\n", trendingKeywords));
		
		List<String> trendingUrls = (new RedisCommand<List<String>>(jedisPool) {
            @Override
            public List<String> build() throws JedisException {	
            	
            	Set<Tuple> set = jedis.zrevrangeWithScores("trending-urls:2014-03-30-16-33", 0, -1);
            	List<String> list = new ArrayList<String>(set.size());
            	for (Tuple e : set) {
            		String url = jedis.hget("url:"+ e.getElement(), "full-url");
            		String str = url+ " " + e.getScore();
            		list.add(str);
				}
                return list;
            }
	    }).execute();
		System.out.println();
		System.out.println(StringUtil.joinFromList("\n", trendingUrls));
	}
}
