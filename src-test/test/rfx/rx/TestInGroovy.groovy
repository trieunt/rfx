package test.rfx.rx;

import rx.Observable;

Observable.from("one", "two", "three")
.take(2)
.subscribe({arg -> println(arg)})

def myObservable = Observable.from([1, 2, 3]);

myObservable.startWith(-3, -2, -1, 0).subscribe(
  [ onNext:{ println(it); },
	onCompleted:{ println("Sequence complete"); },
	onError:{ println("Error encountered"); } ]
);

odds  = Observable.from([1, 3, 5, 7]);
evens = Observable.from([2, 4, 6]);

Observable.concat(odds, evens).subscribe(
  [ onNext:{ println(it); },
	onCompleted:{ println("Sequence complete"); },
	onError:{ println("Error encountered"); } ]
)

// this closure is an Observable that emits three numbers
numbers   = Observable.from([1, 2, 3]);
// this closure is an Observable that emits two numbers based on what number it is passed
multiples = { n -> Observable.from([ n*2, n*3 ]) };

numbers.mapMany(multiples).subscribe(
  [ onNext:{ println(it.toString()); },
	onCompleted:{ println("Sequence complete"); },
	onError:{ println("Error encountered"); } ]
);

numbers = Observable.toObservable([1, 2, 3, 4, 5, 6, 7, 8, 9]);
numbers = numbers.map({a -> a + 1});
numbers.subscribe( { println(it) });
Observable.buffer(numbers, 2).subscribe({ println "bathc: "+ it })

Observable.reduce(numbers, { a, b -> a+b }).subscribe(
  [ onNext:{ println("total: "+it); },
	onCompleted:{ println("Sequence complete"); },
	onError:{ println("Error encountered"); } ]
);