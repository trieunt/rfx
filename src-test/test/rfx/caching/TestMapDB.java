package test.rfx.caching;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.Required;
import org.databene.contiperf.junit.ContiPerfRule;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;

import rfx.core.util.StringUtil;
import rfx.core.util.Utils;

public class TestMapDB {
	
	
	@Rule
	public ContiPerfRule i = new ContiPerfRule();
	
	static DB db = DBMaker.newMemoryDB()
	               .closeOnJvmShutdown()		               
	               .make();
	final static int MAX_SAMPLE_SIZE = 100000;
	static {
		//init();
	}
	
	static void init(){
		//init data
	    HTreeMap<String, Integer> map = db.getHashMap("test1");
	    for (int i = 0; i < MAX_SAMPLE_SIZE; i++) {
			map.put("u"+i, i);
			if(i%10000==0){
				db.commit();
			}
			System.out.println(i);
		}
	    db.commit();
	}
	
	
	@PerfTest(invocations = 1000, threads = 100)
	@Required(max = 600, average = 100)
	public void testInMemoryLookup(){
		String k = "u"+Utils.randomNumber(0, MAX_SAMPLE_SIZE);
		HTreeMap<String, Integer> map = db.getHashMap("test1");
		int v = StringUtil.safeParseInt(map.get(k), -1);
		
        System.out.println(k+" => "+v);  
		Assert.assertTrue(v >= 0);
	}
	
	@Test
	@PerfTest(invocations = 1, threads = 1)	
	public void test1(){	
        int v = 1;
        boolean result = v >= 0;
		Assert.assertTrue(result);
	}
	
	@Test
	@PerfTest(invocations = 1, threads = 1)	
	public void test2(){	
        int v = -1;
        boolean result = v >= 0;		
		if( ! result){
			//make it wrong (red color)
			Utils.sleep(200);			
		}
		Assert.assertTrue(result);
	}
	

}
