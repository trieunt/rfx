//https://github.com/ariya/phantomjs/blob/master/examples/waitfor.js
var args = require('system').args;

if(args.length == 2){
	var page = require('webpage').create();
	page.settings.userAgent = 'SpecialAgent';
	//page.settings.userAgent = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.115 Safari/537.36';
	
	var url = args[1];
	//console.log('The default user agent is ' + page.settings.userAgent);
	
	page.open(url, function (status) {
	    if (status !== 'success') {
	        console.log('Unable to access network');
	    } else {
	        var ua = page.evaluate(function () {
	        	var markup = document.getElementById('search').outerHTML;
	        	//var markup = document.documentElement.innerHTML;
	        	//var markup = document.getElementsByTagName('body')[0].innerHTML;
	            return markup;
	        });
	        console.log(ua);
	    }
	    phantom.exit();
	});
}
