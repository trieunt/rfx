var primeCheck = function(){
	var startTime = new Date().getTime();

	var lowerBound = 2, higherBound = 500000;
	var primecheck = false;

	var primes = [];
	if ((lowerBound >= 2) && (higherBound > lowerBound)) {
		var k;
		for (k = lowerBound; k < higherBound; k++) {
			primecheck = true;
			for (var j = 2; j < k / 2; j++) {
				if (k % j == 0) {
					primecheck = false;
					break;
				}
			}
			if (primecheck) {
				primes.push(k);
			}
		}
	} else {
		return;
	}
	
	if (primes.length > 0) {
		for(var k in primes) {
			console.log(primes[k]);			
		}	
	} else {
		console.log("No primes exist");
	}

	var totalTime = (new Date().getTime()) - startTime;
	var timeStr = '[totalTime in milliseconds is ' + totalTime +"]";
	console.log(timeStr);
	
	return [{list_length:primes.length}, {time:timeStr}];
} 

var express = require('express');
var app = express();
 
app.get('/prime-check', function(req, res) {
    res.send(primeCheck());
});
app.get('/ping', function(req, res) {
    res.send('pong');
}); 
app.listen(3000);
console.log('Listening on port 3000...');

