package rfx.sample.social.tracking;

import rfx.core.cluster.node.BaseWorker;

public class WorkerImpl extends BaseWorker{

	public WorkerImpl() {
		super(TrackingUserTopology.class.getSimpleName()+"Worker");
	}

	@Override
	protected void initWorker() {
		setWorker(this);		
	}
}
