package rfx.sample.social.tracking;

import rfx.core.functor.common.TokenizingFunctor;
import rfx.core.topology.BaseTopology;
import rfx.core.topology.Pipeline;
import rfx.core.util.LogUtil;
import rfx.core.util.Utils;
import rfx.sample.social.tracking.functor.FindingSocialTrends;
import rfx.sample.social.tracking.functor.ParsingSocialActivityLog;


/**
 * tracking user activity and finding social trends (keywords, topics) in real-time
 * @author trieu
 */
public class TrackingUserTopology extends BaseTopology  {
	static final String TOPO_NAME = TrackingUserTopology.class.getSimpleName();
	static final int MAX_POOL_SIZE = 5000;
	
	public TrackingUserTopology() {
		super(TOPO_NAME);
	}
	
	@Override
	public BaseTopology buildTopology(){
		LogUtil.setPrefixFileName("social-activity");		
		return Pipeline.create(MAX_POOL_SIZE, this)
				.apply(TokenizingFunctor.class)
				.apply(ParsingSocialActivityLog.class)
				.apply(FindingSocialTrends.class)	
				.done();
	}
	
	public static void main(String[] args) {
		int beginPartitionId  = 0;
		int endPartitionId  = 1;
		String kafkaTopic = "social-activity";
		
		BaseTopology topology = new TrackingUserTopology();
		topology.initKafkaDataSeeders(kafkaTopic, beginPartitionId, endPartitionId).buildTopology().start();
		Utils.sleep(2000);
	}
}