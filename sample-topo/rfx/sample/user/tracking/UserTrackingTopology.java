package rfx.sample.user.tracking;

import rfx.core.topology.BaseTopology;
import rfx.core.topology.Pipeline;
import rfx.core.util.LogUtil;
import rfx.core.util.Utils;
import rfx.sample.user.tracking.functors.UserTrackingFunctor;
import rfx.sample.user.tracking.functors.UserTrackingLogTokenizer;


public class UserTrackingTopology extends BaseTopology  {

	static final String TOPO_NAME = UserTrackingTopology.class.getSimpleName();
	static final int MAX_POOL_SIZE = 20000;	
	
	public UserTrackingTopology() {
		super(TOPO_NAME);		
	}
	
	@Override
	public BaseTopology buildTopology(){
		return Pipeline.create(MAX_POOL_SIZE, this)
				.apply(UserTrackingLogTokenizer.class)
				.apply(UserTrackingFunctor.class)		
				.done();
	}	
	
	public static void main(String[] args) {
		int beginPartitionId  = 0;
		int endPartitionId  = 1;
		String kafkaTopic = "user-activity";
		LogUtil.setPrefixFileName(kafkaTopic);
		
		BaseTopology topology = new UserTrackingTopology();
		topology.initKafkaDataSeeders(kafkaTopic, beginPartitionId, endPartitionId).buildTopology().start(800);
		Utils.sleep(2000);
	}
}
