package rfx.sample.item.tracking;

import rfx.core.functor.common.TokenizingFunctor;
import rfx.core.topology.BaseTopology;
import rfx.core.topology.Pipeline;
import rfx.core.util.LogUtil;
import rfx.core.util.Utils;
import rfx.sample.item.tracking.functor.ItemLogParsingFunctor;


public class TrackingItemTopology extends BaseTopology  {

	static final String TOPO_NAME = TrackingItemTopology.class.getSimpleName();
	static final int MAX_POOL_SIZE = 5000;	
	
	public TrackingItemTopology() {
		super(TOPO_NAME);		
	}
	
	@Override
	public BaseTopology buildTopology(){
		return Pipeline.create(MAX_POOL_SIZE, this)
				.apply(TokenizingFunctor.class)
				.apply(ItemLogParsingFunctor.class)		
				.done();
	}	
	
	public static void main(String[] args) {
		int beginPartitionId  = 0;
		int endPartitionId  = 1;
		String kafkaTopic = "item-tracking";
		LogUtil.setPrefixFileName(kafkaTopic);
		
		BaseTopology topology = new TrackingItemTopology();
		topology.initKafkaDataSeeders(kafkaTopic, beginPartitionId, endPartitionId).buildTopology().start();
		Utils.sleep(2000);
	}
}
