package rfx.core.functor.common;

import rfx.core.functor.BaseFunctor;
import rfx.core.listener.Metricable;
import rfx.core.message.Fields;
import rfx.core.message.Tuple;
import rfx.core.model.DataFlowInfo;
import rfx.core.processor.TokenProcessor;
import rfx.core.topology.BaseTopology;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;

public class TokenizingFunctor extends BaseFunctor implements Metricable{	
		
	//what data fields that this actor would send to next actor
	static Fields outputFields = new Fields("query","cookie","loggedtime","ip","useragent","topic","partitionId"); 
	TokenProcessor processor;
	
	public TokenizingFunctor(DataFlowInfo dataFlowInfo, BaseTopology topology) {
		super(dataFlowInfo, topology);
		processor = new TokenProcessor();
	}

	public void onReceive(Object message) throws Exception {
		if (message instanceof Tuple) {
			this.doPreProcessing();
			Tuple inputTuple = (Tuple) message;
			//1000 messages, done in (MILLISECONDS):4223
			//20000 messages, done in (MILLISECONDS):26437
			Tuple outTuple = processor.processToTuple(inputTuple, outputFields);
			
			//processed 1000 messages, done in (MILLISECONDS):4641
			//20000 messages, done in (MILLISECONDS):26804
			//20000 messages, done in (MILLISECONDS):25111
			//Tuple outTuple = new TokenProcessor().processTuple(inputTuple, outputMetadataFields);
			
			if(outTuple != null){	
				//output to next phase										
				this.emit(outTuple, self());
				this.counter(StringUtil.toString(this.getMetricKey(), outTuple.getStringByField("topic") + "#" +outTuple.getIntegerByField("partitionId"))).incrementAndGet();
				this.topology.counter().incrementAndGet();
			} else {
			    //iFluentLogger.error(TokenizingActor.class.getName(), "Tokenizing Spout", "logTokens.length (delimiter is tab) is NOT = 5, INVALID LOG ROW FORMAT ");
			    LogUtil.error("logTokens.length (delimiter is tab) is NOT = 5, INVALID LOG ROW FORMAT ");
			} 				
			inputTuple.clear();
		} else {
			unhandled(message);
		}
	}

	@Override
	public String getMetricKey() {		
		return "TokenizingActor#";
	}
}