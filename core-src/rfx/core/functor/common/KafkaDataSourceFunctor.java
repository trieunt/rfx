package rfx.core.functor.common;

import java.util.List;

import rfx.core.functor.BaseFunctor;
import rfx.core.kafka.KafkaCallbackResult;
import rfx.core.kafka.KafkaData;
import rfx.core.message.Fields;
import rfx.core.message.KafkaDataPayload;
import rfx.core.message.Tuple;
import rfx.core.message.Values;
import rfx.core.model.CallbackResult;
import rfx.core.model.DataFlowInfo;
import rfx.core.topology.BaseTopology;
import rfx.core.util.Utils;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class KafkaDataSourceFunctor extends BaseFunctor {
	public static final String EMIT_LOG_EVENT = "EmitLogEvent";
	public static final String STOP_EMIT_LOG_EVENT = "StopEmitLogEvent";
		
	LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	
	public KafkaDataSourceFunctor(DataFlowInfo dataFlowInfo, BaseTopology topology) {
		super(dataFlowInfo, topology);
	}

	//what data fields that this actor would send to next actor
	static Fields fields = new Fields("log_row","topic","partitionId");
	List<KafkaData> logs;
	
	volatile boolean isEmitting = false;
	volatile boolean stopEmitting = false;
	int maxSizeToSleep = 10000;
	long timeToSleep = 1;
	
	@Override
	public void onReceive(Object message) throws Exception {
		//System.out.println(message + " at " + System.currentTimeMillis());
		if (EMIT_LOG_EVENT.equals(message)) {
			if(isEmitting || stopEmitting){
				System.out.println("--------KafkaDataSourceActor.skip-------"+message);
				unhandled(message);
				return;
			}
			
			isEmitting = true;			
			CallbackResult<String> result = super.doPreProcessing();
			if(result != null){
				if(result instanceof KafkaCallbackResult){					
					KafkaDataPayload data = ((KafkaCallbackResult)result).getKafkaDataPayload();
					if(data != null){
						logs = data.getKafkaDataList();
						String topic = data.getTopic();
						int partitionId = data.getPartitionId();
						
						int c = 0;
						for (KafkaData log : logs) {
							Values values =  new Values(log.data(), topic, partitionId);						
							Tuple newTuple = new Tuple(fields, values);	
							this.emit(newTuple, self());
							if(c % maxSizeToSleep == 0){
								Utils.sleep(timeToSleep);
							}
							super.doPostProcessing();
						}
						logs.clear();						
					}					
				}
			}
			isEmitting = false;
	    } else if(STOP_EMIT_LOG_EVENT.equals(message)){
	    	stopEmitting();
	    }		
		else {
	      unhandled(message);
	    }
	}	
	
	public void setMaxSizeToSleep(int maxSizeToSleep) {
		this.maxSizeToSleep = maxSizeToSleep;
	}
	
	public void setTimeToSleep(long timeToSleep) {
		this.timeToSleep = timeToSleep;
	}
	
	public boolean isEmitting() {
		return isEmitting;
	}
	
	public boolean stopEmitting() {
		stopEmitting = true;		
		return stopEmitting;
	}
	
	public boolean restartEmitting() {
		stopEmitting = false;		
		return stopEmitting;
	}
}