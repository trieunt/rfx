package rfx.core.functor;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import rfx.core.cluster.ClusterDataManager;
import rfx.core.message.Tuple;
import rfx.core.model.CallbackResult;
import rfx.core.model.DataFlowInfo;
import rfx.core.topology.BaseTopology;
import rfx.core.util.ActorUtil;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;

/**
 * abstract Functor: functional actor, the interface for reactive data processing entity <br>
 * @author trieu
 */
public abstract class BaseFunctor extends UntypedActor {
	
	protected final DataFlowInfo dataFlowInfo;
	protected final BaseTopology topology;
	static Map<String, AtomicLong> counters = new ConcurrentHashMap<String, AtomicLong>();
	
	static {
		Timer timer = new Timer(true);
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
				ClusterDataManager.logMetrics(counters);				
			}
		}, 10000, 10000);		
	}
		
	protected BaseFunctor(DataFlowInfo dataFlowInfo, BaseTopology topology) {
		super();
		this.dataFlowInfo = dataFlowInfo;
		this.topology = topology;
	}	

	/**
	 * emit a tuple from sender to defined receiver in topology 
	 * 
	 * @param newTuple
	 * @param sender
	 */
	protected final void emit(Tuple newTuple, ActorRef sender) {		
		ActorUtil.sendToActor(dataFlowInfo, newTuple, sender);
	}
	
	/**
	 * emit a tuple from sender to specific receiver 
	 * 
	 * @param newTuple
	 * @param sender
	 * @param namespaceReceiver
	 */
	protected final void emit(Tuple newTuple, ActorRef sender, String namespaceReceiver) {		
		ActorUtil.sendToActor(dataFlowInfo, newTuple, sender, namespaceReceiver);
	}
	
	protected final synchronized CallbackResult<String> doPreProcessing(){
		if(dataFlowInfo.getPreProcessingFunction() != null){
			return dataFlowInfo.getPreProcessingFunction().call();
		}
		return null;
	}
	
	protected final synchronized CallbackResult<String> doPostProcessing(){
		if(dataFlowInfo.getPostProcessingFunction()!= null){
			return dataFlowInfo.getPostProcessingFunction().call();
		}
		return null;
	}
	protected final synchronized AtomicLong counter(String metricName) {
		AtomicLong counter = counters.get(metricName);
		if(counter == null){
			counter = new AtomicLong(0);
			counters.put(metricName, counter);
		}
		return counter;
	}
}
