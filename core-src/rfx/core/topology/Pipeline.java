package rfx.core.topology;

import java.util.Map;

import rfx.core.model.DataFlowInfo;
import akka.actor.ActorRef;

/**
 * 
 * simplify building pipeline of topology
 * 
 * @author trieu
 *
 */
public class Pipeline {
	DataFlowInfo senderinfo;
	int defaultPoolSize = 5000;
	BaseTopology topology;
	
	protected Pipeline(int defaultPoolSize, BaseTopology topology) {
		super();
		this.defaultPoolSize = defaultPoolSize;
		this.topology = topology;
	}

	public static Pipeline create(int defaultPoolSize, BaseTopology topology){
		return new Pipeline(defaultPoolSize, topology);
	}
	
	public static Pipeline create(BaseTopology topology){
		return new Pipeline(5000, topology);
	}
		
	public Pipeline apply(Class<?> functorClass){
		return apply(functorClass, defaultPoolSize);
	}
	
	public Pipeline apply(Class<?> functorClass, int poolSize){
		DataFlowInfo dfInfo = new DataFlowInfo(functorClass);
		Map<String, ActorRef> actorsPool = topology.createActorPool(functorClass,dfInfo, poolSize);
		
		if(senderinfo == null){
			//the first will be received logs from Kafka Data Seeders
			topology.setReceiverFromEmitter(functorClass, actorsPool);
		} else {
			senderinfo.addReceiverActorPool(functorClass, actorsPool);
		}
		senderinfo = dfInfo;		
		return this;
	}
	
	public BaseTopology done(){
		senderinfo = null;
		return topology;
	}
}
