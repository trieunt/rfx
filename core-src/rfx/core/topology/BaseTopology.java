package rfx.core.topology;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import rfx.core.cluster.ClusterDataManager;
import rfx.core.configs.KafkaTopologyConfig;
import rfx.core.emitters.DataEmitter;
import rfx.core.functor.common.KafkaDataSourceFunctor;
import rfx.core.kafka.KafkaCallbackResult;
import rfx.core.kafka.KafkaDataEmitter;
import rfx.core.kafka.KafkaDataQuery.QueryFilter;
import rfx.core.kafka.KafkaDataSeeder;
import rfx.core.message.KafkaDataPayload;
import rfx.core.model.Callback;
import rfx.core.model.CallbackResult;
import rfx.core.model.DataFlowInfo;
import rfx.core.model.DataFlowPostProcessing;
import rfx.core.util.ActorUtil;
import rfx.core.util.CommonUtil;
import rfx.core.util.LogUtil;
import rfx.core.util.WorkerUtil;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Cancellable;
import akka.actor.Props;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * The BaseTopology for actor-based topology
 * 
 * @author trieu
 *
 */
public abstract class BaseTopology {

	static Config config; 
	static {		
		try {
			config = ConfigFactory.parseFile(new File(CommonUtil.ACTOR_SYSTEM_CONFIG_FILE));
		} catch (Exception e) {
			System.err.println("Can NOT load file "+CommonUtil.ACTOR_SYSTEM_CONFIG_FILE);
			System.exit(1);
		}
	}
	
	protected ActorSystem system;
	protected String topologyName;
	private int totalActors = 0;
	
	//common for data emitters
	protected List<KafkaDataSeeder> kafkaDataSeeders; 
	protected List<DataEmitter> dataEmitters = new ArrayList<>();	
	protected Map<String, ActorRef> emittingDataActors;
	protected Map<String, Cancellable> emitterCancels;
	
	//control how system will shutdown safely
	protected volatile boolean shutdownSystem = false;
	
	//logging metrics to redis
	private Map<String, AtomicLong> counters = new ConcurrentHashMap<String, AtomicLong>();
	private static Timer metricsTimer = new Timer(true);
	
	//the topology flow of system 
	protected Map<Integer, DataFlowInfo> topologyFlows = new TreeMap<>();
		
	public BaseTopology(String topologyName) {
		super();
		this.topologyName = topologyName;
		system = ActorSystem.create(this.topologyName,config);
		this.kafkaDataSeeders = new ArrayList<>();
	}
	
	public BaseTopology initKafkaDataSeeders(String topic, int beginPartitionId, int endPartitionId){
		List<KafkaDataSeeder> dataSeeders = new ArrayList<>(); 
		for (int partition = beginPartitionId; partition <= endPartitionId; partition++) {
			dataSeeders.add(new KafkaDataSeeder(topic, partition));
		}	
		return setKafkaDataSeeders(dataSeeders);
	}
	
	public BaseTopology setKafkaDataSeeders(List<KafkaDataSeeder> dataSeeders) {
		if(this.kafkaDataSeeders == null){
			this.kafkaDataSeeders = dataSeeders;
		} else {
			this.kafkaDataSeeders.addAll(dataSeeders);
		}
		return this;
	}

	public void shutdownTopologyActorSystem(){
		shutdownSystem = true;
		metricsTimer.cancel();
		cancelScheduleEmittingData();		
		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		executor.schedule(new Runnable() {			
			@Override
			public void run() {				 
				system.shutdown();		
				
				//shutdown other services
				LogUtil.shutdownLogThreadPools();
				WorkerUtil.autoSystemExit("Shutdown actor system OK! BYE BYE ...", 1000);
			}
		},4,TimeUnit.SECONDS);
		executor.shutdown();
	}
	
	public synchronized AtomicLong counter() {		
		return counter(topologyName);
	}
	
	public ActorSystem getSystem() {
		return system;
	}
	
	public Map<Integer, DataFlowInfo> getTopologyFlows() {
		return topologyFlows;
	}
	
	public synchronized AtomicLong counter(String metricName) {
		AtomicLong counter = counters.get(metricName);
		if(counter == null){
			counter = new AtomicLong(0);
			counters.put(metricName, counter);
		}
		return counter;
	}
	
	public List<DataEmitter> getDataEmitters() {
		return dataEmitters;
	}
	
	public BaseTopology addDataEmitter(DataEmitter dataEmitter) {
		dataEmitters.add(dataEmitter);
		return this;
	}
	
	public Map<String, ActorRef> getEmittingDataActors() {
		return emittingDataActors;
	}
		
	public List<DataFlowInfo> defineDataEmitter(Class<?> clazzSender){
		if(dataEmitters.size() == 0){
			initDataEmitters(this);
		}		
		List<DataFlowInfo> emitflows = new ArrayList<>(dataEmitters.size());
		//emit log actors		
		int i = 0;
		for (DataEmitter kafkaDataEmitter : dataEmitters) {
			i++;
			DataFlowInfo emitflowInfo = new DataFlowInfo(clazzSender, String.valueOf(i));
			emitflowInfo.setPreProcessingFunction(kafkaDataEmitter);
			emitflowInfo.setPostProcessingFunction(new DataFlowPostProcessing() {				
				@Override
				public CallbackResult<String> call() {
					counter("DataEmitter").incrementAndGet();
					return null;
				}
			});
			emitflows.add(emitflowInfo);
		}
		this.emittingDataActors = ActorUtil.createEmitterPool(this, KafkaDataSourceFunctor.class, emitflows);
		return emitflows;
	}
	
	public void setReceiverFromEmitter( Class<?> clazzReceiver, Map<String, ActorRef> poolReceiver){		
		List<DataFlowInfo> emitflows = defineDataEmitter(DataEmitter.class);
		for (DataFlowInfo dataFlowInfo : emitflows) {
			dataFlowInfo.addReceiverActorPool(clazzReceiver, poolReceiver);	
		}	
	}
	
	
	public BaseTopology scheduleEmittingData(long period){		
		return scheduleEmittingData(500, period);
	}
	
	public BaseTopology scheduleEmittingData(long delay, long period){
		if(emittingDataActors == null){
			throw new IllegalStateException("Please calling defineDataEmitter before calling scheduleEmittingData");
		}
		if(shutdownSystem){
			return this;
		} else {
			String emitLogEvent = KafkaDataSourceFunctor.EMIT_LOG_EVENT;
			this.emitterCancels = ActorUtil.scheduleEmittingData(system, emittingDataActors, delay, period, emitLogEvent);
		}
		metricsTimer.schedule(new TimerTask() {			
			@Override
			public void run() {
				ClusterDataManager.logMetrics(counters);
			}
		}, 1000, 5000);
		return this;
	}
	
	public int cancelScheduleEmittingData(){
		if(emitterCancels != null){
			ActorUtil.cancelScheduleEmittingData(this.emitterCancels);
			return emitterCancels.size();
		}		
		if(emittingDataActors != null){
			ActorUtil.killAllActorsInPool(system, emittingDataActors);
		}
		return 0;
	}
	
	public boolean isCounterOverLimit(long limit){
		return this.counter().longValue() >= limit;
	}
	
	public boolean whenCounterOverLimit(long limit, Callback<String> callback){
		boolean rs = this.counter().longValue() >= limit;
		if(rs && callback != null){
			callback.call();
		}
		return rs;
	}
	
	protected void initDataEmitters(final BaseTopology topology){
		if(kafkaDataSeeders.isEmpty()){
			throw new IllegalArgumentException("dataSeeders.isEmpty = true");
		}
		mappingDataSeederToEmitter(topology, kafkaDataSeeders);
	}
	
	private void mappingDataSeederToEmitter(final BaseTopology topology, List<KafkaDataSeeder> dataSeeders){
		for (KafkaDataSeeder dataSeeder : dataSeeders) {
			DataEmitter dataEmitter = new KafkaDataEmitter(dataSeeder) {			
				@Override
				public synchronized CallbackResult<String> call() {
					KafkaDataPayload dataPayload = this.myKafkaDataSeeder().buildQuery().seedData();
					topology.counter(getMetricKey()).addAndGet(dataPayload.size());
					return new KafkaCallbackResult(dataPayload);
				}
				@Override
				public String getMetricKey() {
					return "emitter#"+this.myKafkaDataSeeder().getTopic()+"#"+this.myKafkaDataSeeder().getPartition();
				}
			};
			dataEmitters.add(dataEmitter);
		}
	}
	
	protected void mappingDataSeederToEmitter(final BaseTopology topology, final QueryFilter queryFilter){
		if(kafkaDataSeeders == null || dataEmitters == null){
			LogUtil.error("dataSeeders or kafkaDataEmitters is NULL");
			return;
		}
		if(kafkaDataSeeders.size() == 0){
			return;
		}
		for (KafkaDataSeeder dataSeeder : kafkaDataSeeders) {
			DataEmitter dataEmitter = new KafkaDataEmitter(dataSeeder) {			
				@Override
				public synchronized CallbackResult<String> call() {
					KafkaDataPayload dataPayload = this.myKafkaDataSeeder().buildQuery(queryFilter).seedData();
					topology.counter(getMetricKey()).addAndGet(dataPayload.size());
					return new KafkaCallbackResult(dataPayload);
				}
				@Override
				public String getMetricKey() {
					return "emitter#"+this.myKafkaDataSeeder().getTopic()+"#"+this.myKafkaDataSeeder().getPartition();
				}
			};
			dataEmitters.add(dataEmitter);
		}
	}
	
	public Map<String, ActorRef> createActorPool( Class<?> clazz, DataFlowInfo dataFlowInfo, int maxPoolSize) {
		Map<String, ActorRef> actorPool = new HashMap<>(maxPoolSize);
		for (int i = 0; i < maxPoolSize; i++) {
			String id = dataFlowInfo.getNamespaceSender() + i;		
			Props props = Props.create(clazz,dataFlowInfo,this);
			ActorRef actor = system.actorOf(props, id);
			actorPool.put(id, actor);
		}		
		//keep the ordering of flows
		topologyFlows.put(topologyFlows.size()+1, dataFlowInfo);
		totalActors+=maxPoolSize;
		System.out.println("---createActorPool OK for:"+clazz.getName() + ",size:"+actorPool.size());
		return actorPool;
	}
	
	protected Map<String, ActorRef> createActorPool(Class<?> clazz, int maxPoolSize) {
		return createActorPool(clazz, new DataFlowInfo(clazz), maxPoolSize);
	}

	public String getTopologyFlowsAsString() {
		StringBuilder s = new StringBuilder();
		Collection<DataFlowInfo> flows = this.topologyFlows.values();
		for (DataFlowInfo flow : flows) {
			s.append(flow.toString()).append(" ");
		}
		return s.toString().trim();
	}
	
	public int getTotalActors() {
		return totalActors;
	}

	public static BaseTopology lookupTopologyFromTopic(String topic){		
		// lookup the classpath for the topic
		String classpath = KafkaTopologyConfig.getTopologyClassPath(topic);
		if(classpath != null){
			System.out.println("For topic:" + topic + " , begin create topology: "	+ classpath);					
			try {
				@SuppressWarnings("unchecked")
				Class<BaseTopology> clazz = (Class<BaseTopology>) Class.forName(classpath);
				BaseTopology topology = clazz.newInstance();
				return topology;
			}  catch (Exception e) {
				if (e instanceof ClassNotFoundException) {
					System.err.println(e.getClass().getName() + " for classpath:" + classpath + " by topic:" + topic);
				} 
				LogUtil.error(e);
			}
		}
		return null;
	}
	
	protected void defaultShutdownKafkaWorker(){
		if(shutdownSystem){
			return;
		}

		//stop seeding data
		KafkaDataSeeder.stopSeedingAndWait(kafkaDataSeeders);
		
		//shutdown actor system after 5 seconds
		this.shutdownTopologyActorSystem();		
	}
	
	public void shutdownAll(){
		this.defaultShutdownKafkaWorker();
	}

	//abstract class for a implementation
	public abstract BaseTopology buildTopology();
	
	public void start(){
		this.scheduleEmittingData(1600);
	}
	
	public void start(long period){
		this.scheduleEmittingData(period);
	}
}
