package rfx.core.model;

import java.io.Serializable;

public class VisitorMetricData implements Serializable {

	private static final long serialVersionUID = -2684536391958250761L;
	int totalClick;
	int totalImpression;
	int totalTrueImpression;
	int totalPageview;
	String visitorId;
	int lastActivityTime;
	boolean isOnline = false;
	
	public String getVisitorId() {
		return visitorId;
	}

	public void setVisitorId(String visitorId) {
		this.visitorId = visitorId;
	}

	public VisitorMetricData(String visitorId, boolean isOnline) {
		super();
		this.visitorId = visitorId;
		this.isOnline = isOnline;
	}	
	
	public boolean isOnline() {
		return isOnline;
	}
	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}

	public int getTotalClick() {
		return totalClick;
	}

	public void setTotalClick(int totalClick) {
		this.totalClick = totalClick;
	}

	public int getTotalImpression() {
		return totalImpression;
	}

	public void setTotalImpression(int totalImpression) {
		this.totalImpression = totalImpression;
	}

	public int getTotalTrueImpression() {
		return totalTrueImpression;
	}

	public void setTotalTrueImpression(int totalTrueImpression) {
		this.totalTrueImpression = totalTrueImpression;
	}

	public int getTotalPageview() {
		return totalPageview;
	}

	public void setTotalPageview(int totalPageview) {
		this.totalPageview = totalPageview;
	}

	public int getLastActivityTime() {
		return lastActivityTime;
	}

	public void setLastActivityTime(int lastActivityTime) {
		this.lastActivityTime = lastActivityTime;
	}
	
	
}