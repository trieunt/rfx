package rfx.core.model;

import java.util.Calendar;
import java.util.Date;

import rfx.core.util.DateTimeUtil;
import rfx.core.util.StringPool;

public class RawLogRecord {

	static final String DAY_NAME_TOKEN = "/day=";
	static final String HOUR_NAME_TOKEN = "/hour=";
	static final String RAW_PREFIX = "/raw-log-";
	
	private String partitionId;
	private int unixloggedtime;
	private String rawLogFileFolder;
	private String rawLogFileName;
	private String logData;
	
	public RawLogRecord(String partitionId, int unixloggedtime, String logData) {
		this.partitionId = partitionId ;
		this.unixloggedtime = unixloggedtime;
		this.logData = logData;
		_parseRawLogFileName();
	}
	
	private void _parseRawLogFileName(){
		Date loggedDate = new Date(unixloggedtime * 1000L);
		String dateFolder = DateTimeUtil.formatDate(loggedDate, "yyyy-MM-dd");
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(loggedDate);
		int slot = (int) Math.floor(calendar.get(Calendar.MINUTE) / 15);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		String hourFolder = (hour < 10) ? ("0" + hour) : "" + hour;
		
		String dirHour = DAY_NAME_TOKEN + dateFolder + HOUR_NAME_TOKEN + hourFolder ;
		rawLogFileFolder = dirHour ; // rawLogFileFolder = /day=2013-10-21/hour=03
		
		StringBuilder s = new StringBuilder();
		s.append(RAW_PREFIX).append(dateFolder);
		s.append(StringPool.MINUS).append(hourFolder);
		s.append(StringPool.MINUS).append(partitionId);
		s.append(StringPool.MINUS).append(slot);
		
		rawLogFileName = s.toString();// rawLogFileName = /raw-log-2013-10-21-03-{slot}
	}
	
	public int getUnixloggedtime() {
		return unixloggedtime;
	}

	public void setUnixloggedtime(int unixloggedtime) {
		this.unixloggedtime = unixloggedtime;
	}
	
	public String getLogData() {
		return logData;
	}

	public void setLogData(String logData) {
		this.logData = logData;
	}

	public String getRawLogFileName() {
		return rawLogFileName;
	}
	
	public String getRawLogFileFolder() {
		return rawLogFileFolder;
	}

	public String getPartitionId() {
		return partitionId;
	}

	public void setPartitionId(String partitionId) {
		this.partitionId = partitionId;
	}
	
}