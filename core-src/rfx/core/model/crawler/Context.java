package rfx.core.model.crawler;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

/**
 *
 * @author tantrieuf31
 */
public class Context {
    String location;
    List<Integer> bestTimes = new ArrayList<>();

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<Integer> getBestTimes() {
        return bestTimes;
    }

    public void setBestTimes(List<Integer> bestTimes) {
        this.bestTimes = bestTimes;
    }
    
    public void addBestTimes(int unixtime) {
        this.bestTimes.add(unixtime);
    }
    

    @Override
    public String toString() {
    	return new Gson().toJson(this);
    }
}
