package rfx.core.listener;

/**
 * for measure everything in an actor or topology
 * 
 * @author trieu
 *
 */
public interface Metricable {

	public String getMetricKey();
}
