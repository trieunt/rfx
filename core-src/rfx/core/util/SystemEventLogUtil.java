package rfx.core.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import rfx.core.configs.ClusterInfoConfigs;
import rfx.core.model.SystemEventData;

public class SystemEventLogUtil {
	static String systemLogEventFile = ClusterInfoConfigs.load().getSystemEventDbPath() + "/system-events";
	static DB db = DBMaker.newFileDB(new File(systemLogEventFile)).closeOnJvmShutdown().make();
	static ConcurrentNavigableMap<Long, SystemEventData> map = db.getTreeMap("SystemEventData");

	public static void log(SystemEventData log) {
		map.put(log.getLoggedTime(), log);
		db.commit();
	}

	public static List<SystemEventData> getTop10() {
		Map<Long, SystemEventData> map = db.getTreeMap("SystemEventData");
		List<SystemEventData> list = new ArrayList<>(map.values());
		int limit = (list.size() > 10) ? 10 : list.size();
		if(limit>0){
			list = list.subList(0, limit);
		}
		return list;
	}

	public static void main(String[] args) throws InterruptedException, IOException {
		for (int i = 0; i < 20; i++) {
			SystemEventData SystemEventData = new SystemEventData();
			SystemEventData.setOrder(i);
			SystemEventData.setSource("ClickTopo");
			SystemEventData.setLoggedTime(System.currentTimeMillis());
			SystemEventData.setEventDetails("Finish campaign "+i);
			SystemEventLogUtil.log(SystemEventData);
			Thread.sleep(1000);
		}

		List<SystemEventData> list = SystemEventLogUtil.getTop10();
		for (SystemEventData SystemEventData : list) {
			System.out.println(SystemEventData.getLoggedTime());
		}
	}
}
