package rfx.core.util.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadExecutor {

	int maxSizeToClean = 100;
	int maxConcurrentThread = 1;
	int counter = 0;
	volatile boolean isShutdown = false, locked = false;
	
	ExecutorService executor;

	public ThreadExecutor(int maxSizeToClean, int maxConcurrentThread) {
		super();
		this.maxSizeToClean = maxSizeToClean;
		this.maxConcurrentThread = maxConcurrentThread;
		this.executor = Executors.newFixedThreadPool(maxConcurrentThread);
	}
	
	public ThreadExecutor(int maxSizeToClean) {
		super();
		this.maxSizeToClean = maxSizeToClean;		
		this.executor = Executors.newSingleThreadExecutor();
	}
	
	public void execute(Runnable command){
		if(isShutdown){
			command.run();
			return;
		}
		try {
			if(locked){
				command.run();
				return;
			}
			executor.execute(command);
			counter++;
			if(counter>=maxSizeToClean){
				locked = true;
				executor.shutdown();	
				executor = null;
				if(maxConcurrentThread > 1){
					this.executor = Executors.newFixedThreadPool(maxConcurrentThread);	
				} else {
					this.executor = Executors.newSingleThreadExecutor();
				}
				locked = false;
			}
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	public void shutdown(){
		if(!isShutdown){
			try {
				isShutdown = true;
				executor.shutdown();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
