package rfx.core.util;

public class MemoryManagementUtil {
	// http://www.concretepage.com/java/

	private static long[] fm = new long[5];
	private static long[] tm = new long[5];
	private static long[] mm = new long[5];
	private static long[] um = new long[5];

	public static void main(String[] a) {
		Runtime rt = Runtime.getRuntime();
		
		
		
		getMemoryInfo(rt, 0);
		getMemoryInfo(rt, 1);
		getMemoryInfo(rt, 2);
		getMemoryInfo(rt, 3);
		getMemoryInfo(rt, 4);
		printMemoryInfo(rt, 0);
		printMemoryInfo(rt, 1);
		printMemoryInfo(rt, 2);
		printMemoryInfo(rt, 3);
		printMemoryInfo(rt, 4);
		for (int i = 0; i < 5; i++) {
			System.out.println("Get memory info - step = " + i);
			System.out.println("   Free memory = " + fm[i]);
			System.out.println("   Total memory = " + tm[i]);
			System.out.println("   Maximum memory = " + mm[i]);
			System.out.println("   Memory used = " + um[i]);
		}
	}

	public static void getMemoryInfo(Runtime rt, int i) {
		fm[i] = rt.freeMemory();
		tm[i] = rt.totalMemory();
		mm[i] = rt.maxMemory();
		um[i] = tm[i] - fm[i];
	}

	public static void printMemoryInfo(Runtime rt, int i) {
		long fm = rt.freeMemory();
		long tm = rt.totalMemory();
		long mm = rt.maxMemory();
		System.out.println("Print memory info - step = " + i);
		System.out.println("   Free memory = " + fm);
		System.out.println("   Total memory = " + tm);
		System.out.println("   Maximum memory = " + mm);
		System.out.println("   Memory used = " + (tm - fm));
	}
}
