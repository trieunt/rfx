package rfx.core.util.io;

public class StdOut {
	public final static StdOut i = new StdOut();
	private StdOut() {}

	public static StdOut println(Object o){
		System.out.println(o);
		return i;
	}
	
	public static StdOut print(Object o){
		System.out.print(o);
		return i;
	}
	
	public static StdOut println(){
		System.out.println();
		return i;
	}
	
	public static StdOut println(Object o, boolean atWebConsole){
		System.out.println(o);
		return i;
	}
}
