package rfx.core.util;

import java.io.File;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;

import rfx.core.configs.WorkerConfigs;
import rfx.core.model.VisitorMetricData;


public class MapDbUtil {
	
	public static abstract class MapDbCallback {
		public abstract void apply(ConcurrentNavigableMap<String,Object> kafkaOffsetDb) ;
	}
	
	public static void connectMapDb(String dbName, String mapDbFilePath, MapDbCallback mapDbCallback) {
		//Kafka Offset Storage
		DB mapDb = null;
		ConcurrentNavigableMap<String,Object> kafkaOffsetDb = null;
		try {			
			File mapDbFile = new File(mapDbFilePath);				
			if( ! mapDbFile.isFile() ){
				boolean ok = mapDbFile.createNewFile();
				if( !ok ){
					throw new IllegalAccessError("Can NOT createNewFile: "+mapDbFilePath);
				}
			}
			
			mapDb = DBMaker.newFileDB(mapDbFile).closeOnJvmShutdown().make();
			kafkaOffsetDb = mapDb.getTreeMap(dbName);
			mapDbCallback.apply(kafkaOffsetDb);						
			mapDb.commit();
		} catch (Throwable e) {
			e.printStackTrace();
			LogUtil.e("KafkaConfigManager", e.toString());			
			WorkerUtil.autoSystemExit(300);
		} finally {
			if(kafkaOffsetDb == null){
				LogUtil.e("KafkaConfigManager", "kafkaOffsetDb is NULL, failed at mapDb.getTreeMap(\""+dbName+"\") ");
			}	
			if(mapDb != null){
				mapDb.close();
			}
		}		
	}
	
	public static int persistToMapDb(ConcurrentMap<String, VisitorMetricData> map ){		
		DB mapDb = null;
		HTreeMap<String, VisitorMetricData> kafkaOffsetDb;
		String kafkaOffsetDir =  WorkerConfigs.load().getKafkaOffsetDbPath();
		try {
			String date = DateTimeUtil.getDateStringForDb(new Date());
			String fullPath =  kafkaOffsetDir + "/users-"+date;
			File file = new File(fullPath);
			if( ! file.exists() ){
				file.createNewFile();				
			}
			LogUtil.i("persistToMapDb at path: "+file.getAbsolutePath());
			mapDb = DBMaker.newFileDB(file).make();
			kafkaOffsetDb = mapDb.getHashMap("users");
			
			Set<String> keys = map.keySet();
			for (String key : keys) {
				VisitorMetricData v = map.get(key);
				if(v != null){
					kafkaOffsetDb.put(key, v);
				}
			}
			mapDb.commit();
		} catch (Throwable e) {
			if(e instanceof java.io.IOException){
				LogUtil.e("persistToMapDb", kafkaOffsetDir + " is NOT valid folder");
			} else {
				e.printStackTrace();
				LogUtil.e("persistToMapDb", e.toString());
			}
		} finally {
			mapDb.close();
		}		
		return map.size();
	}
}
