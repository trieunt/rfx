package rfx.core.util;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import rfx.core.cluster.ClusterDataManager;
import rfx.core.cluster.node.KafkaProcessorNode;
import rfx.core.configs.WorkerConfigs;
import rfx.core.functor.CoordinatingFunctor;
import rfx.core.functor.handler.BaseCoordinatingAction;
import rfx.core.kafka.KafkaPartitionOffset;
import rfx.core.message.WorkerPayload;
import rfx.core.model.WorkerInfo;
import scala.concurrent.Await;
import scala.concurrent.Future;
import akka.actor.ActorIdentity;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Identify;
import akka.actor.Props;
import akka.pattern.AskableActorSelection;
import akka.util.Timeout;

import com.typesafe.config.Config;

public class SupervisorUtil {
	
	static final Map<String, WorkerInfo> workerInfoCache2 = new ConcurrentHashMap<>();
	private static boolean checkCallBackFinished;	
		
	
	public static Map<String, WorkerInfo> getManagedWorkers() {
		if(workerInfoCache2.size()>0){
			return workerInfoCache2;
		}
		WorkerConfigs workerConfigs = WorkerConfigs.load();	
		String prefixWorkerName = workerConfigs.getPrefixWorkerName();
		String host = workerConfigs.getHostName();
		List<WorkerInfo> tcpPorts = workerConfigs.getAllocatedWorkers();		
		//Map<String, WorkerInfo> workerInfos = new HashMap<>();
		String type = KafkaProcessorNode.TYPENAME;
		for (WorkerInfo w : tcpPorts) {
			String name = StringUtil.toString(type, prefixWorkerName, "_", host.replaceAll("\\.", ""), "_" , w.getPort());
			workerInfoCache2.put(name, new WorkerInfo(name, host,  w.getPort()));
		}
		return workerInfoCache2;
	}
	
			
	
	/**
	 * remember close ActorSystem after usage
	 * @return ActorSystem
	 */
	public static ActorSystem getCmdActorSystem() {
		boolean kt = false;
		ActorSystem actorSystem = null;
		try {
			do {
				int port = ActorUtil.getPortGernaralCommandActor();
				String host = InetAddress.getLocalHost().getHostAddress();
				try {
					Config actorConfig = ActorUtil.getCmdActorConfig(host,port).getConfig("CmdActor");
					actorSystem = ActorSystem.create("CmdActor", actorConfig);
					kt = true;
				} catch (Exception ex) {
					kt = false;
				}
			} while ( ! kt );
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return actorSystem;
	}
	
	public static boolean ping(WorkerInfo workerInfo){
		try {
			int timeout = 300;
			Socket socket = new Socket();
			socket.connect(new InetSocketAddress(workerInfo.getHost(), workerInfo.getPort()), timeout);
			socket.close();		
			return true ;
		} catch (Exception ex) {}	
		return false;
	}
	
	
	public static boolean isRemoteActorAlive(String workerName) {
		return isRemoteActorAlive(ClusterDataManager.getWorkerInfo(workerName));
	}
	
	public static boolean isRemoteActorAlive(WorkerInfo workerInfo) {
		ActorSystem system = null;
		boolean rs = false;
		try {			
			if(workerInfo == null){
				return false;
			}
			String host = workerInfo.getHost(); int port = workerInfo.getPort();
			
			String path = ActorUtil.buildRemotePath(workerInfo.getName(), host, port);	                	
			system = getCmdActorSystem();
        	Timeout t = new Timeout(5, TimeUnit.SECONDS);
        	AskableActorSelection asker = new AskableActorSelection(system.actorSelection(path));
        	Future<Object> fut = asker.ask(new Identify(1), t);        	
        	ActorIdentity ident =  (ActorIdentity) Await.result(fut,t.duration());
			ActorRef ref = ident.getRef();
			rs = ref != null;
			System.out.println("#### " + path + " = " + rs);
		} catch (Throwable e) {
			 e.printStackTrace();
		} finally {
			if(system != null){
				system.shutdown();
			}
			Utils.sleep(100);
		}
		return rs;
	}
	
	public static boolean kill(String workerName) {
		WorkerInfo workerInfo = ClusterDataManager.getWorkerInfo(workerName);
		return sendToRemoteActor(workerInfo, new WorkerPayload<String>(WorkerPayload.SHUTDOWN));
	}
	
	
	public static int killAllWorkers(boolean isCheck) {
		Map<String, WorkerInfo> hostInfoMap = ClusterDataManager.getWorkerInfoFromRedis();
		Set<String> workerNames = hostInfoMap.keySet();
		for (String workerName : workerNames) {
			kill(workerName);
		}
		if(isCheck){
		    Utils.sleep(1000);
		    for (String workerName : workerNames) {
				boolean kt = checkWorkerStatus(hostInfoMap.get(workerName));
				System.out.println("Worker " + workerName + " is: " + kt);
		    }
		}		
		return workerNames.size();
	}
	
	public static boolean checkWorkerStatus(String workerName) {
		return checkWorkerStatus(ClusterDataManager.getWorkerInfo(workerName));		
	}
	public static boolean checkWorkerStatus(WorkerInfo hostInfo) {
		try {
			if(hostInfo == null){
				return false;
			}			
			boolean alive = SupervisorUtil.isRemoteActorAlive(hostInfo);
			hostInfo.setAlive(alive);
			ClusterDataManager.saveWorkerInfo(hostInfo);
			return alive;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}	
	
	public static Map<String, Object> getKafkaOffsetAtWorker(final String workerName, final String kafkaTopic){
	    
	    final Map<String, Object> result = new HashMap<>();
	    checkCallBackFinished = false;
	    
	    BaseCoordinatingAction<KafkaPartitionOffset> senderHandler = new BaseCoordinatingAction<KafkaPartitionOffset>() {
			@Override
			public Object action(WorkerPayload<KafkaPartitionOffset> dataPayload, ActorRef self, ActorRef sender) {
			    	System.out.println("Get kafka offset partition : "+dataPayload.getCmd());
				List<KafkaPartitionOffset> list = dataPayload.getMessages();
				for (KafkaPartitionOffset kafkaPartitionOffset : list) {
				    String partition = "Client_" + kafkaTopic + "_" +kafkaPartitionOffset.getPartitionId();
				    Object offset = (Object) kafkaPartitionOffset.getOffset();
				    
				    result.put(partition, offset);
				}
				
				checkCallBackFinished = true;
				System.out.println("checkCallBackFinished=" +checkCallBackFinished);
				return null;
			}
		};	
		
		String cmd = "request-kafka-offset: "+ new Date();	
		WorkerPayload<KafkaPartitionOffset> payload =  new WorkerPayload<KafkaPartitionOffset>(cmd);
		payload.setTopic(kafkaTopic);
		WorkerInfo workerInfo = ClusterDataManager.getWorkerInfo(workerName);
		boolean alive = SupervisorUtil.sendToRemoteActor(workerInfo, payload, senderHandler);
		long startTime = System.currentTimeMillis();
		long endTime;
		while(!checkCallBackFinished){		    
			//FIXME with future
			Utils.sleep(5);
		    endTime = System.currentTimeMillis();
		    long deplayTime = endTime - startTime;
		    if(deplayTime > 2000){
		    	break;
		    }
		}
		
		return result;
		    
	}
	
	public static void setKafkaOffset(final String workerName, final String kafkaTopic, final String kafkaOffsets){
	    checkCallBackFinished = false;
	    
	    BaseCoordinatingAction<String> senderHandler = new BaseCoordinatingAction<String>() {
		@Override
		public Object action(WorkerPayload<String> dataPayload, ActorRef self, ActorRef sender) {
		    	System.out.println("Get kafka offset partition : "+dataPayload.getCmd());
			List<String> listResult = dataPayload.getMessages();	
			
			checkCallBackFinished = true;
			System.out.println("checkCallBackFinished=" +checkCallBackFinished + ", listResult=" + listResult);
			return null;
			}
                };	
        	
  
        	String cmd = "set-kafka-offset: "+ new Date();	
        	WorkerPayload<String> payload =  new WorkerPayload<String>(cmd);
        	payload.setTopic(kafkaTopic);
        	payload.addMessage(kafkaOffsets);
        	WorkerInfo workerInfo = ClusterDataManager.getWorkerInfo(workerName);
        	boolean alive = SupervisorUtil.sendToRemoteActor(workerInfo, payload, senderHandler);        	
        	
        	long startTime = System.currentTimeMillis();
        	long endTime;
        	while(!checkCallBackFinished){
        		//FIXME with future
    			Utils.sleep(5);
        	    endTime = System.currentTimeMillis();
        	    long deplayTime = endTime - startTime;
        	    if(deplayTime > 2000){
        		break;
        	    }
        	}
    	
    	}
	
	public static int setKafkaTopic(final String workerName, final String kafkaTopic, int beginPartition, int endPartition){	    
       final long startTime1 = System.currentTimeMillis();
       BaseCoordinatingAction<String> senderHandler = new BaseCoordinatingAction<String>() {
            @Override
        	public Object action(WorkerPayload<String> dataPayload, ActorRef self, ActorRef sender) {
    			long timespan = System.currentTimeMillis() - startTime1;
		    	System.out.println("SupervisorUtil.setKafkaTopic : "+dataPayload.getCmd() + ". timespan=" + timespan);				
		    	checkCallBackFinished = true ;
		    	return null;
			}
	    };	
	
    	String cmd = "set-topology-for-worker:"+ new Date();	
    	WorkerPayload<String> payload =  new WorkerPayload<String>(cmd,3);
    	payload.setTopic(kafkaTopic);
    	payload.addMessage(StringUtil.toString(beginPartition));
    	payload.addMessage(StringUtil.toString(endPartition));
    	payload.addMessage("false");		
    	WorkerInfo workerInfo = ClusterDataManager.getWorkerInfo(workerName);
    	boolean alive = SupervisorUtil.sendToRemoteActor(workerInfo, payload, senderHandler);
    	if(!alive){
    	    return 1;
    	}
	
    	long startTime = System.currentTimeMillis();
    	long endTime;
    	while(!checkCallBackFinished){
    		//FIXME with future
			Utils.sleep(5);
    	    endTime = System.currentTimeMillis();
    	    long deplayTime = endTime - startTime;
    	    if(deplayTime > 2000){
    		return 2;
    	    }
    	}
    	return 0;	    
    }
	
	public static <T> boolean sendToRemoteActor(String workerName, WorkerPayload<T> payload, BaseCoordinatingAction<T> senderHandle) {
		return sendToRemoteActor(ClusterDataManager.getWorkerInfo(workerName),payload,senderHandle);
	}

	public static <T> boolean sendToRemoteActor(WorkerInfo hostInfo, WorkerPayload<T> payload, BaseCoordinatingAction<T> senderHandle) {
		ActorSystem system = null;
		boolean rs = false;
		ActorRef senderRef = null;
		try {
			if(hostInfo == null){
				return false;
			}
			String remoteWorkerName = hostInfo.getName();
			String host = hostInfo.getHost(); int port = hostInfo.getPort();
			String path = ActorUtil.buildRemotePath(remoteWorkerName, host, port);
			system = getCmdActorSystem();
        	Timeout t = new Timeout(2, TimeUnit.SECONDS);
        	AskableActorSelection asker = new AskableActorSelection(system.actorSelection(path));
        	Future<Object> fut = asker.ask(new Identify(1), t);        	
            ActorIdentity ident =  (ActorIdentity) Await.result(fut,t.duration());
			ActorRef ref = ident.getRef();
			rs = ref != null;
			if (rs) {
				//if PING ok, then send data to remote actor 
				if(senderHandle == null){
					//send without handler
					ref.tell(payload, ActorRef.noSender());
				} else {
					//send with handler
					senderRef = system.actorOf(Props.create(CoordinatingFunctor.class, senderHandle));
					ref.tell(payload, senderRef);
				}
			}
			System.out.println("#### " + path + " = " + rs);
		} catch (Exception e) {			
			e.printStackTrace();
		} finally {
			if (system != null) {
				if(senderHandle != null){	
					Utils.sleep(2000);
					if(senderRef != null){
						system.stop(senderRef);
					}
				} 
				system.shutdown();
			}
			Utils.sleep(10);
		}
		return rs;
	}
	
	public static <T>  boolean sendToRemoteActor(WorkerInfo hostInfo,  WorkerPayload<T> payload) {
		return sendToRemoteActor(hostInfo, payload, null);
	}

}
