package rfx.core.util;

import groovy.lang.Binding;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

import java.io.File;
import java.io.IOException;
import java.util.Map;


public class GroovyScriptUtil {

	static GroovyScriptEngine groovyScriptEngine;
	static String basePath = "src-script/scheduled-jobs/";
	static {
		try {
			groovyScriptEngine = new GroovyScriptEngine(new String[] {basePath});
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Can not init GroovyScriptEngine!");
			WorkerUtil.autoSystemExit(200);
		}	
	}	
	
	public static String runAllInScheduledJobScripts(){
		File dir = new File(basePath);
		File[] scripts = dir.listFiles();
		StringBuilder s = new StringBuilder();
		for (File script : scripts) {
			String scriptName = script.getName(); 
			s.append(runScript(scriptName, null)).append("\n");
		}
		return s.toString();
	}
	
	public static String runScript(String scriptName, Map<String, String[]> params){
		String output= "";
		try {
			Binding binding = new Binding();
			if(params != null){
				binding.setVariable("params", params);
			}
			groovyScriptEngine.run(scriptName, binding);
			output = binding.getVariable("output").toString();
		} catch (ResourceException e) {			
			return "runScript: " + scriptName + " is not found!";
		} catch (ScriptException e) {			
			e.printStackTrace();
		}		
		return output;
	}
	
	public static void main(String[] args) {
		String out = GroovyScriptUtil.runScript("synch-data.groovy", null);
		System.out.println(out);
	}

}
