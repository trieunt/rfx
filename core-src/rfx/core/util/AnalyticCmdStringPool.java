package rfx.core.util;

public class AnalyticCmdStringPool {

	public static String SUBMIT = "cmd:submit";
	public static String ACTIVATE = "cmd:activate";
	public static String DEACTIVATE = "cmd:deactivate";
	public static String KILL = "cmd:kill";
	public static String SHUTDOWN = "cmd:shutdown";
	
	public static String REGISTER_WORKER = "cmd:register-worker";
	public static String ASSIGN_WORKER = "cmd:assign-worker";
}
