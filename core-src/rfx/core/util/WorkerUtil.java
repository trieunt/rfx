package rfx.core.util;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import rfx.core.configs.SqlDbConfigs;
import rfx.core.data.DbGenericDao;


public class WorkerUtil {
	public static boolean checkingDWHConfigs(){
		// Connect to Oracle Database
		System.out.println("2) Check Oracle DWH connection ...");
		boolean check = false;
		try {			
			Connection con = DbGenericDao.getOracleDataSource().getConnection();
			Statement statement = con.createStatement();			
			ResultSet rs = statement.executeQuery("SELECT SYSDATE FROM DUAL");

			if (rs.next()) {
				String currentDbDate = rs.getDate(1).toString(); // get first column returned				
				String devSysDate = DateTimeUtil.formatDate(new java.util.Date(), "yyyy-MM-dd");
				if( devSysDate.equals(currentDbDate)){
					System.out.println(" - Current Date from DWH is : " + currentDbDate);
					System.out.println(" - Current Date from Storm server is : " + devSysDate);
					System.out.println(" - "+ "Connection is OK");
					check = true;
				}
			}
			rs.close();
			statement.close();
			con.close();
		} catch (Exception e) {			
			e.printStackTrace();
			System.err.println(" - "+ "Connection is FAIL");
		}
		return check;
	}
	
	public static boolean checkRedisConfigs(){
		System.out.println("1) Check Redis connections ...");
		boolean check = true;
		//RedisPoolConfigs redisConfigs = RedisPoolConfigs.load();
		
		//TODO
		
		return check;
	}
	
	
	public static boolean checkingMySqlConnection(){		
		System.out.println("3) Check MySQL Campaign Database connection ...");
		boolean check = false;	
		Connection con = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		try {
			con = SqlDbConfigs.load("dbMySqlCampaignsConfigs").getDataSource().getConnection();
			cs = con.prepareCall( "SELECT SYSDATE() AS SYSTEM_CURRENT_DATE");			

			rs = cs.executeQuery();			

			if (rs.next()) {		
				String currentDbDate = rs.getDate(1).toString(); // get first column returned				
				String devSysDate = DateTimeUtil.formatDate(new java.util.Date(), "yyyy-MM-dd");
				if( devSysDate.equals(currentDbDate)){
					System.out.println(" - Current Date from MySQL is : " + currentDbDate);
					System.out.println(" - Current Date from Storm server is : " + devSysDate);
					System.out.println(" - "+ "Connection is OK");
					check = true;
				}
			}

		} catch (SQLException e) {			
			e.printStackTrace();
		} finally {
			if(rs != null){
				try {
					rs.close();
				} catch (SQLException e) {}	
			}
			if(cs != null){
				try {
					cs.close();
				} catch (SQLException e) {}	
			}
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {}	
			}
		}
		return check;
	}
	
	public static void autoSystemExit(int timeout){
		autoSystemExit("Exit",timeout);
	}
	
	public static void autoSystemExit(final String msg, int timeout){		
		if(timeout <= 1){
			System.out.println("\n"+msg);
			System.exit(1);
			return;
		}
		
		ScheduledExecutorService executor =  Executors.newSingleThreadScheduledExecutor();
		executor.schedule(new Runnable() {			
			@Override
			public void run() {				 
				System.out.println("\n"+msg);
				System.exit(1);
			}
		},timeout,TimeUnit.MILLISECONDS);
		executor.shutdown();
	}
}
