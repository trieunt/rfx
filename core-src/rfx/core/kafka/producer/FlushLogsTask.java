package rfx.core.kafka.producer;

import java.util.List;
import java.util.concurrent.TimeUnit;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import rfx.core.kafka.KafkaProducerUtil;
import rfx.core.util.LogUtil;

import com.google.common.base.Stopwatch;

public class FlushLogsTask implements Runnable  {
	
	List<KeyedMessage<String, String>> batchLogs;
	private String actorId;
	Producer<String, String> producer;

	public FlushLogsTask(String actorId, Producer<String, String> producer, List<KeyedMessage<String, String>> batchLogs) {		
		this.actorId = actorId;
		this.producer = producer;
		this.batchLogs = batchLogs;
		if(this.batchLogs == null){
			throw new IllegalArgumentException("batchLogs CAN NOT BE NULL");
		}
	}
	
	@Override
	public void run() {			
		if(producer != null && batchLogs.size()>0){
			try {		
				Stopwatch stopwatch = new Stopwatch().start();			
				producer.send(batchLogs);			
				stopwatch.stop();				
				long milis = stopwatch.elapsedTime(TimeUnit.MILLISECONDS);
				System.out.println(this.actorId+" batchsize = "+batchLogs.size()+" to Kafka in milisecs = "+milis+"\n");	
			} catch (Exception e) {
				e.printStackTrace();
				LogUtil.e("FlushLogsTask", "sendToKafka fail : "+e.getMessage());	
				//close & open the Kafka Connection manually				
				KafkaProducerUtil.closeAndRemoveKafkaProducer(actorId);
			} finally {
				batchLogs.clear();
				batchLogs = null;
			}	
		} else {
			LogUtil.e("FlushLogsTask", "producer is NULL for actorId:" + actorId);
		}
		
	}
}