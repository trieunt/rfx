package rfx.core.kafka.producer;


public interface KafkaLogHandler {
	
	
	public static final String logPageviewImpressionKafka = "kafka-pageview-impression-Producer";
	public static final String logTrueImpressionKafka = "kafka-trueimpression-Producer";
	public static final String logClickKafka = "kafka-click-Producer";	
	

	/**
	 * Asynchronous push log data queue, the timer will schedule a job for sending to Kafka to avoid locking response
	 * 
	 * @param ip
	 * @param request
	 */
	public abstract void writeLogToKafka(String ip, String userAgent, String logDetails, String cookieString);	
	public abstract void flushAllLogsToKafka(); 

	
}