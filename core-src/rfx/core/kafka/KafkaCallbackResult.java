package rfx.core.kafka;

import rfx.core.message.KafkaDataPayload;
import rfx.core.model.CallbackResult;

public class KafkaCallbackResult extends CallbackResult<String> {

	KafkaDataPayload kafkaDataPayload;
	
	public KafkaCallbackResult(KafkaDataPayload kafkaDataPayload) {
		super();
		this.kafkaDataPayload = kafkaDataPayload;
	}

	public KafkaDataPayload getKafkaDataPayload() {
		return kafkaDataPayload;
	}

	public void setKafkaDataPayload(KafkaDataPayload kafkaDataPayload) {
		this.kafkaDataPayload = kafkaDataPayload;
	}
	
}
