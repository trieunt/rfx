package rfx.core.nosql.graph;

public class Edge<T> implements Comparable<Edge<T>> , java.io.Serializable {
	
	private static final long serialVersionUID = -2976781146927794031L;
	T pubNode;
	T subNode;
	float weight = -1;
	
	
	public Edge(T pubNode, T subNode) {
		super();
		this.pubNode = pubNode;
		this.subNode = subNode;
	}
	
	
	public Edge(T pubNode, T subNode, float weight) {
		super();		
		this.pubNode = pubNode;
		this.subNode = subNode;
		this.weight = weight;
	}


	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public T getPubNode() {
		return pubNode;
	}
	public void setPubNode(T pubNode) {
		this.pubNode = pubNode;
	}
	public T getSubNode() {
		return subNode;
	}
	public void setSubNode(T subNode) {
		this.subNode = subNode;
	}


	@Override
	public int compareTo(Edge<T> o) {
		if(this.getWeight() > o.getWeight()){
			return 1;
		} else if(this.getWeight() < o.getWeight()){
			return -1;
		}
		return 0;
	}
	
	@Override
	public int hashCode() {
		return (""+pubNode+subNode).hashCode();
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		if(pubNode != null){
			s.append(pubNode);
			if(weight>=0){
				s.append("--(").append(weight).append(")-->");
			} else {
				s.append("-->");
			}
		}
		if(subNode != null){
			s.append(subNode);
		}
		return s.toString();
	}

}
