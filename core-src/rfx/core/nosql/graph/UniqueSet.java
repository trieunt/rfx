package rfx.core.nosql.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class UniqueSet<T> implements java.util.Set<T> {
	
	private Map<Integer,T> set;
		

	public UniqueSet(int max) {
		super();
		this.set = new HashMap<Integer, T>(max);
	}

	@Override
	public int size() {		
		return set.size();
	}

	@Override
	public boolean isEmpty() {
		return set.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return set.containsKey(o.hashCode());
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return set.values().iterator();
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return set.values().toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return set.values().toArray(a);
	}

	@Override
	public boolean add(T e) {
		// TODO Auto-generated method stub
		return set.put(e.hashCode(), e) == null;
	}

	@Override
	public boolean remove(Object o) {
		return set.remove(o.hashCode()) != null;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return set.values().containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {	
		return set.values().addAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {		
		return set.values().retainAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {		
		return set.values().removeAll(c);
	}

	@Override
	public void clear() {
		set.clear();
	}
	
	public List<T> sort(Comparator<T> comparator){
		Collection< T > collection = set.values();
		List<T> list = new ArrayList<T>( collection );
		Collections.sort( list ,comparator);
		return list;
	}

}
