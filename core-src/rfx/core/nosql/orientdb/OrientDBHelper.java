package rfx.core.nosql.orientdb;

import java.io.IOException;
import java.util.List;


import com.orientechnologies.orient.client.db.ODatabaseHelper;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;

public class OrientDBHelper<T> {
    public static OSQLSynchQuery<ODocument> createQuery(String qry) {
	return new OSQLSynchQuery<ODocument>(qry);
    }

    public static OSQLSynchQuery<ODocument> createQuery(String qry, int limit) {
	return new OSQLSynchQuery<ODocument>(qry, limit);
    }

    public boolean updatePageviewDataHourly(List<T> list) {
	System.setProperty("ORIENTDB_HOME", "/home/dungth5/Downloads/orientdb-community-1.6.3");
	ODatabaseDocumentTx db = new ODatabaseDocumentTx("remote:localhost/pageview");
	boolean ok = false;
	try {
	    // check database exits if !exits, create database
	    if (!ODatabaseHelper.existsDatabase(db, "plocal")) {
		ODatabaseHelper.createDatabase(db, "remote:localhost/pageview", "/home/dungth5/Downloads/orientdb-community-1.6.3", "plocal");
	    }

	    db.open("root", "FAFF343DD54DKFJFKDA95F05A");

	    // check class exits if !exits, create class
	    if (!db.getMetadata().getSchema().existsClass("OPageview")) {
		OClass pageviewClass = db.getMetadata().getSchema().createClass("OPageview");
		pageviewClass.createProperty("logtime", OType.DATETIME);
		pageviewClass.createIndex("OPageview.logtime", OClass.INDEX_TYPE.NOTUNIQUE, "logtime");
		pageviewClass.createProperty("websiteid", OType.STRING);
		pageviewClass.createProperty("url", OType.STRING);
		
		String sql = "CREATE INDEX OPageview.pv ON OPageview (logtime, websiteid, url) UNIQUE";
		OCommandSQL createIndex = new OCommandSQL(sql);
		db.command(createIndex).execute(new Object[0]);
		
		pageviewClass.createProperty("pageview", OType.INTEGER);
		db.getMetadata().getSchema().save();
	    }

	    // save or update pageview
	    for (T data : list) {
			OSQLSynchQuery<ODocument> query = createQuery("select from OPageview where logtime = ? and websiteid = ? and url = ?");
//			List<ODocument> result = db.command(query).execute(data.getLoggedTimeString(), data.getWebsiteId(), data.getUrl());
//			if (result != null && result.size() == 1) {
//			    result.get(0).field("pageview", data.getPageview());
//			    result.get(0).save();
//			} else {
//			    ODocument doc = new ODocument("OPageview");
//			    doc.field("logtime", data.getLoggedTimeString());
//			    doc.field("websiteid", data.getWebsiteId());
//			    doc.field("url", data.getUrl());
//			    doc.field("pageview", data.getPageview());
//			    doc.save();
//			}
			ok = true;
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    db.close();
	}

	return ok;
    }
}
