package rfx.core.cluster.node.handler;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.vertx.java.core.MultiMap;

import rfx.core.cluster.ClusterDataManager;
import rfx.core.configs.KafkaTopologyConfig;
import rfx.core.model.http.HttpServerRequestCallback;
import rfx.core.util.HttpResponseUtil;
import rfx.core.util.SystemEventLogUtil;

import com.google.gson.Gson;

/**
 * the HTTP Request Handlers for Master Node
 * 
 * <br>
 * @author trieu
 *
 */
public class MasterNodeHttpHandlers extends HttpHandlerMapper {
	
	public static void init() {
			
	}

	@Override
	public HttpHandlerMapper buildCallbackHandlers() {
		when("/server-time", new HttpServerRequestCallback(){
			@Override
			protected String onHttpGetOk() {
				return new Date().toString();
			}			
		});				
		when("/json/get-kafka-topic", new HttpServerRequestCallback(){
			@Override
			protected String onHttpGetOk() {
				List<String> kafkaTopics = KafkaTopologyConfig.getKafkaTopic();
				Map<String, Object> mapkafkaTopics = new HashMap<>(1);
				mapkafkaTopics.put("kafkaTopics", (Object) kafkaTopics);
				System.out.println("get-kafka-topic "+mapkafkaTopics);
				return HttpResponseUtil.responseAsJsonp("", mapkafkaTopics);
			}			
			@Override
			protected String onError(Throwable e) {				
				return "get-kafka-topic; error got "+e.getMessage();
			}
		});
		when("/json/get-workers", new HttpServerRequestCallback(){
			@Override
			protected String onHttpGetOk() {	
				return new Gson().toJson(ClusterDataManager.getWorkerData());
			}			
		});
		when("/json/get-system-logged-events", new HttpServerRequestCallback(){
			@Override
			protected String onHttpGetOk() {	
				return new Gson().toJson(SystemEventLogUtil.getTop10());
			}			
		});
		when("/json/handle-master-request", new HttpServerRequestCallback(){
			@Override
			protected String onHttpGetOk() {	
				String action = this.request.params().get("action");
				System.out.println("action: " + action);
				String data = "";
				return data;
			}			
		});
		when("/json/deploy-topology", new HttpServerRequestCallback(){
			@Override
			protected String onHttpPostOk(MultiMap formData) {
				String base64data = formData.get("base64data");
				String token = formData.get("token");
				String filename = formData.get("filename")+"-"+System.currentTimeMillis();
				//System.out.println("base64data "+base64data);
				System.out.println("token "+token);
				ClusterDataManager.deployJarFileFromBase64Data(base64data, filename);
				return "ok:"+token;
			}
		});	
		return this;
	}
	
}
