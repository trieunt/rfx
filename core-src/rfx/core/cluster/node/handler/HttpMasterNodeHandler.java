package rfx.core.cluster.node.handler;

import static org.jboss.netty.handler.codec.http.HttpHeaders.is100ContinueExpected;
import static org.jboss.netty.handler.codec.http.HttpHeaders.isKeepAlive;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.CONNECTION;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.CONTENT_LENGTH;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import static org.jboss.netty.handler.codec.http.HttpResponseStatus.CONTINUE;
import static org.jboss.netty.handler.codec.http.HttpVersion.HTTP_1_1;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.handler.codec.http.DefaultHttpResponse;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.jboss.netty.handler.codec.http.QueryStringDecoder;
import org.jboss.netty.util.CharsetUtil;

import rfx.core.cluster.ClusterDataManager;
import rfx.core.model.crawler.Item;
import rfx.core.search.LuceneSearchManager;
import rfx.core.util.HttpResponseUtil;
import rfx.core.util.RfxRecommendationUtil;
import rfx.core.util.SystemEventLogUtil;
import rfx.core.util.io.FileUtils;

import com.google.gson.Gson;

/**
 * the netty http handler for master node
 * 
 * @author trieu
 *
 */
@Deprecated
public class HttpMasterNodeHandler extends SimpleChannelUpstreamHandler {

	private HttpRequest request;
	private HttpResponseStatus httpStatus = HttpResponseStatus.OK;

	/** Buffer that stores the response content */
	private final StringBuilder buf = new StringBuilder();
	static final String NOT_FOUND_404 = "";

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e)
			throws Exception {
		HttpRequest request = this.request = (HttpRequest) e.getMessage();
		httpStatus = HttpResponseStatus.OK;

		if (is100ContinueExpected(request)) {
			send100Continue(e);
		}
		buf.setLength(0);// reset
		
		// resource handle
		String uri = request.getUri();
        if (uri.contains("/img")) {		
            new HttpStaticFileServerHandler().messageReceived(ctx, e);
            return;
        }

		// handle
		String contentType = routingHandler(request);

		// response
		writeResponse(e, contentType);
	}

	private void writeResponse(MessageEvent e, String contentType) {
		boolean keepAlive = isKeepAlive(request);
		HttpResponse response = new DefaultHttpResponse(HTTP_1_1, httpStatus);
		response.setContent(ChannelBuffers.copiedBuffer(buf.toString(),	CharsetUtil.UTF_8));
		
		response.addHeader(CONTENT_TYPE, contentType);

		if (keepAlive) {
			response.addHeader(CONTENT_LENGTH, response.getContent().readableBytes());
			response.addHeader(CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
		}

		ChannelFuture future = e.getChannel().write(response);
		if (!keepAlive) {
			future.addListener(ChannelFutureListener.CLOSE);
		}
	}

	private static void send100Continue(MessageEvent e) {
		HttpResponse response = new DefaultHttpResponse(HTTP_1_1, CONTINUE);
		e.getChannel().write(response);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e)
			throws Exception {
		e.getCause().printStackTrace();
		e.getChannel().close();
	}
	
	String readResourceFile(String filename) {
		try {
			return FileUtils.readFileAsString("./resources" + filename);
		} catch (IOException e) {}
		System.err.println("NOT_FOUND_404 request-filename: "+filename);
		httpStatus = HttpResponseStatus.NOT_FOUND;
		return NOT_FOUND_404;
	}

	String routingHandler(HttpRequest request) {
		String contentType = "text/html; charset=UTF-8";
		String uri = request.getUri();
		QueryStringDecoder queryStringDecoder = new QueryStringDecoder(uri);
        Map<String, List<String>> parameter = queryStringDecoder.getParameters();
		
		if(uri.contains("/tracking")){
			//System.out.println(uri);
		}
		else if(uri.contains("/bookmark")){
			String url = HttpResponseUtil.getParamValue("url", parameter,"");
			String name = HttpResponseUtil.getParamValue("name", parameter,"");
			String imageUrl = HttpResponseUtil.getParamValue("imageUrl", parameter,"");			
			String contextLocation = HttpResponseUtil.getParamValue("location", parameter,"");
			
			Item item = new Item(url, name, imageUrl);
			item.setContextLocation(contextLocation);
			LuceneSearchManager.get().indexAndStoreItems(item);
		}
		
		else if (uri.endsWith(".css")) {
			contentType = "text/css";
			buf.append(readResourceFile(uri));
		} else if (uri.endsWith(".js")) {
			contentType = "application/x-javascript";
			buf.append(readResourceFile(uri));
		} else if (uri.equals("/") || uri.equals("")) {			
			buf.append(readResourceFile("/html/cluster-info.html"));
		} else if (uri.endsWith(".html")) {
			buf.append(readResourceFile("/html/"+uri));
		} else if (uri.endsWith("workers.json")) {			
			contentType = "application/json";
			buf.append(new Gson().toJson(ClusterDataManager.getWorkerData()));                        
		} else if (uri.contains("/ajax")) {
			String data = HttpResponseUtil.responseAsJsonp("", HttpMasterAjaxHandler.ajaxHandler(uri));
			buf.append(data);
		} else if(uri.contains("/suggest-me")){
            contentType = "application/x-javascript";           
         
            String callback = HttpResponseUtil.getParamValue("callback", parameter,"");
            //String q = HttpResponseUtil.getParamValue("q",parameter);
            String userId = HttpResponseUtil.getParamValue("userId",parameter);
            String keywords = HttpResponseUtil.getParamValue("actions",parameter);
            String context = HttpResponseUtil.getParamValue("context",parameter);
                                
            HashMap<String,String> obj = new HashMap<>(2);
            obj.put("status", "ok");
            obj.put("data", RfxRecommendationUtil.suggestMe(userId, keywords, context));
            if(callback.isEmpty()){
            	buf.append(new Gson().toJson(obj));
            } else {
            	buf.append(callback).append("(").append(new Gson().toJson(obj)).append(")");
            }            
		} else if (uri.indexOf("/system-events") >= 0) {
			buf.append((new Gson().toJson(SystemEventLogUtil.getTop10())));
		} 
		else {
			System.err.println("NOT_FOUND_404 request-uri: "+uri);
			httpStatus = HttpResponseStatus.NOT_FOUND;
			buf.append(NOT_FOUND_404);
		}
		return contentType;
	}	

}