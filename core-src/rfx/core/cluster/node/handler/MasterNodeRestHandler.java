package rfx.core.cluster.node.handler;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import rfx.core.cluster.ClusterDataManager;
import rfx.core.configs.KafkaTopologyConfig;
import rfx.core.util.HashUtil;
import rfx.core.util.HttpResponseUtil;
import rfx.core.util.SystemEventLogUtil;
import rfx.core.util.io.FileUtils;

import com.google.gson.Gson;

@Path("/json")
@Deprecated
public class MasterNodeRestHandler {
	@Context
	private UriInfo uriInfo;

	@GET
	@Path("handle-master-request")
	@Produces(MediaType.APPLICATION_JSON)
	public String handle(@DefaultValue("nothing") @QueryParam("action") String action) {	
		System.out.println("action: " + action);
		String data = "";
		return data;
	}

	@GET
	@Path("get-kafka-topic")
	@Produces(MediaType.APPLICATION_JSON)
	public String getKafkaTopic() {
		Map<String, Object> responseData = new HashMap<String, Object>(2);
		List<String> kafkaTopics = KafkaTopologyConfig.getKafkaTopic();
		Map<String, Object> mapkafkaTopics = new HashMap<>(1);
		mapkafkaTopics.put("kafkaTopics", (Object) kafkaTopics);

		String data = HttpResponseUtil.responseAsJsonp("", mapkafkaTopics);
		responseData.put("status", "ok");
		responseData.put("msg", data);
		
		return data;
	}
	
	@GET
	@Path("get-workers")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorkers() {
		return new Gson().toJson(ClusterDataManager.getWorkerData());
	}
	
	@GET
	@Path("get-system-logged-events")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSystemLoggedEvents() {
		return new Gson().toJson(SystemEventLogUtil.getTop10());
	}
	
	@POST
	@Path("deploy-topology")
	@Produces(MediaType.APPLICATION_JSON)
	public String deployTopology(
			@DefaultValue("") @FormParam("base64data") String base64data
			,@DefaultValue("") @FormParam("token") String token
			,@DefaultValue("") @FormParam("filename") String filename
			) {
		System.out.println("base64data: "+ base64data);
		System.out.println("token: "+ token);
		System.out.println("filename: "+ filename);
		
		ClusterDataManager.deployJarFileFromBase64Data(base64data, filename);
		//SupervisorUtil.killAllWorkers(true);//kill and worker will be restarted by supervisor
		return new Gson().toJson("OK");
	}

	
	public static void main(String[] args) throws IOException {
		String localFilepath = "/home/trieu/git-projects/rfx-src/rfx/deploy/worker-tracking-user.jar";
		String jarBase64Data = FileUtils.encodeFileToBase64Binary(localFilepath );
		System.out.println(jarBase64Data);
		System.out.println(HashUtil.md5(jarBase64Data));
		//UEsDBAoAAAgAAGi7kUQAAAAAAAAAAAAAAAAJA
		//78b94b92bb248b577b44897049d88222
		
	}
}
