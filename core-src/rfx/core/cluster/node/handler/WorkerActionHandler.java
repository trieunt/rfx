package rfx.core.cluster.node.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rfx.core.cluster.node.KafkaProcessorNode;
import rfx.core.cluster.node.WorkerNode;
import rfx.core.exception.InvalidSetTaskDef;
import rfx.core.functor.handler.BaseCoordinatingAction;
import rfx.core.kafka.KafkaDataSeeder;
import rfx.core.kafka.KafkaPartitionOffset;
import rfx.core.message.WorkerPayload;
import rfx.core.model.KafkaTaskDef;
import rfx.core.util.StringUtil;
import akka.actor.ActorRef;

/**
 * 
 * the Coordinating Action Handler of worker
 * 
 * @author trieu
 *
 */
@Deprecated
public class WorkerActionHandler {
	
	public static BaseCoordinatingAction<String> getWorkerActions() {
		return workerActions;
	}

	static BaseCoordinatingAction<String> workerActions = new BaseCoordinatingAction<String>() {
		@Override
		public Object action(WorkerPayload<String> dataPayload, ActorRef self, ActorRef sender) {
			
			String cmd = dataPayload.getCmd();
			System.out.println("WorkerActionHandler got message: "+ cmd);
			if (cmd.contains("-ack-")) {
				System.out.println("WorkerNode.BaseCoordinatingAction.sender.path:" + sender.path());
				sender.tell(new WorkerPayload<String>("acked: "+ cmd.replace("-ack-", "")), self);
			}
			if (cmd.equalsIgnoreCase("PING")) {
				this.responseToSender("PONG");
			} else if (cmd.equalsIgnoreCase(WorkerPayload.SHUTDOWN)) {
				WorkerNode.getTheInstance().shutdown();
			} else if (cmd.equalsIgnoreCase("update-offset")) {
				updateOffset(dataPayload);
			} else if (cmd.startsWith("request-kafka-offset")) {
				sender.tell(requestKafkaOffsetHandler(dataPayload), self);
			} else if (cmd.startsWith("set-kafka-offset")) {
				sender.tell(setKafkaOffsetHandler(dataPayload), self);
			} else if(cmd.startsWith("set-topology-for-worker")){
				sender.tell(setTopologyForWorkerHandler(dataPayload), self);
			}
			return null;
		}
	};
	
	static void updateOffset(WorkerPayload<String> dataPayload){
		String topic = dataPayload.getTopic();
		List<String> list = dataPayload.getMessages();
		Map<String, Long> offsets = new HashMap<>(list.size());
		for (String tok : list) {
			String[] toks = tok.split("=");
			offsets.put(toks[0], StringUtil.safeParseLong(toks[1]));
		}
		KafkaDataSeeder.updateOffset(topic, offsets, WorkerNode.getWorkerName());
	}
	
	static WorkerPayload<String> setTopologyForWorkerHandler(WorkerPayload<String> dataPayload){
		String topic = dataPayload.getTopic();
		List<String> list = dataPayload.getMessages();
		int beginPartitionId = StringUtil.safeParseInt(list.get(0));
		int endPartitionId = StringUtil.safeParseInt(list.get(1));
		boolean autoStart =  Boolean.parseBoolean(list.get(2));
		WorkerNode node = KafkaProcessorNode.getTheInstance();
		try {
			node.setTaskDef( new KafkaTaskDef(topic, beginPartitionId, endPartitionId, autoStart) );
		} catch (InvalidSetTaskDef e) {
			String m = StringUtil.toString("setTopologyForWorker-fail:",topic," by [",e.getMessage(),"]");
			return new WorkerPayload<>(m);
		}
		String m = StringUtil.toString("setTopologyForWorker-ok:",topic," at worker:",WorkerNode.getWorkerName());
		return new WorkerPayload<>(m);
	}

	static WorkerPayload<String> setKafkaOffsetHandler(
			WorkerPayload<String> dataPayload) {
		String cmd = dataPayload.getCmd().replace("set-", "response-");
		String kafkaTopic = dataPayload.getTopic();
		String kafkaOffsets = dataPayload.getMessage(0);
		String[] offsetPartition = kafkaOffsets.split(",");

		Map<String, Long> offsets = new HashMap<>();
		String key;
		for (int i = 0; i < offsetPartition.length; i++) {
			String[] offsetItem = offsetPartition[i].split("=");
			key = "Client_" + kafkaTopic + "_" + offsetItem[0];
			offsets.put(key, StringUtil.safeParseLong(offsetItem[1]));
		}
		KafkaDataSeeder.updateOffset(kafkaTopic, offsets, WorkerNode.getWorkerName());

		WorkerPayload<String> dataPayload2 = new WorkerPayload<>(cmd, 1);
		dataPayload2.addMessage("success");
		return dataPayload2;
	}

	static WorkerPayload<KafkaPartitionOffset> requestKafkaOffsetHandler(
			WorkerPayload<String> dataPayload) {
		String cmd = dataPayload.getCmd().replace("request-", "response-");
		String kafkaTopic = dataPayload.getTopic();

		Map<String, Long> mapKafkaOffset = KafkaDataSeeder.getOffsets(kafkaTopic, WorkerNode.getWorkerName());
		System.out.println("mapdata:" + mapKafkaOffset + ", topic:"	+ kafkaTopic);
		WorkerPayload<KafkaPartitionOffset> dataPayload2 = new WorkerPayload<>(cmd, mapKafkaOffset.size());

		if (mapKafkaOffset != null) {
			for (Map.Entry<String, Long> entry : mapKafkaOffset.entrySet()) {
				try {
					String partitionKey = entry.getKey();
					partitionKey = partitionKey.substring(partitionKey.lastIndexOf("_") + 1);
					int partition = StringUtil.safeParseInt(partitionKey);
					long offset = (Long) entry.getValue();

					KafkaPartitionOffset data = new KafkaPartitionOffset(
							partition, offset, kafkaTopic);
					dataPayload2.addMessage(data);
				} catch (ClassCastException e) {
				}
			}
		}
		return dataPayload2;
	}

}