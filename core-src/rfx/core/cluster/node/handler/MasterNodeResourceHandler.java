package rfx.core.cluster.node.handler;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import rfx.core.util.StringPool;
import rfx.core.util.io.FileUtils;


@Path("/")
@Deprecated
public class MasterNodeResourceHandler {
	@Context
	private UriInfo uriInfo;
	
	final static String BASE_RESOURCE = "./resources";
	
	String readResourceFile(String filename) {
		try {
			return FileUtils.readFileAsString(BASE_RESOURCE + filename);
		} catch (IOException e) {}
		System.err.println("NOT_FOUND_404 request-filename: "+filename);				
		return "";
	}
	
	@GET	
	@Produces("text/html; charset=utf-8")
	public String index() {		
		return readResourceFile("/html/index.html");
	}
	
	@GET
	@Path("/pages/{path: [-.a-zA-Z0-9_/]+}")
	@Produces("text/html; charset=utf-8")
	public String getPages(@PathParam("path") String path) {
		System.out.println(uriInfo.getPath());
		return readResourceFile("/html/"+uriInfo.getPath());
	}
	
	@GET	
	@Path("/cluster-info")
	@Produces("text/html; charset=utf-8")
	public String clusterInfo() {		
		return readResourceFile("/html/cluster-info.html");
	}
	
	@GET
	@Path("/html/{path: [-.a-zA-Z0-9_/]+}")
	@Produces("text/html; charset=utf-8")
	public String getHtml(@PathParam("path") String path) {
		System.out.println(uriInfo.getPath());
		return readResourceFile("/"+uriInfo.getPath());
	}
	
	@GET
	@Path("/css/{path: [-.a-zA-Z0-9_/]+}")
	@Produces("text/css")
	public String getCss(@PathParam("path") String path) {
		System.out.println("getContentType "+ FileUtils.getContentType(path) );		
		return readResourceFile("/"+uriInfo.getPath());
	}
	
	@GET
	@Path("/js/{path: [-.a-zA-Z0-9_/]+}")	
	public Response getJavaScript(@PathParam("path") String path) {
		System.out.println("js path "+path);
		File f = new File( BASE_RESOURCE +"/js/"+ path);
		System.out.println(f.getPath());
		
		if (!f.exists()) {
		    throw new WebApplicationException(404);
		}
		
		String ct;
		if(path.endsWith(".js")){
			ct = StringPool.MediaType.JAVASCRIPT;
		} else if(path.endsWith(".css")){
			ct = StringPool.MediaType.CSS;
		} else {
			ct = StringPool.MediaType.TEXT;
		}
		return Response.ok(f, ct).build();
	}

}
