package rfx.core.cluster.node;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import rfx.core.cluster.ClusterDataManager;
import rfx.core.configs.KafkaTopologyConfig;
import rfx.core.configs.WorkerConfigs;
import rfx.core.model.WorkerData;
import rfx.core.model.WorkerInfo;
import rfx.core.topology.BaseTopology;
import rfx.core.util.StringUtil;
import rfx.core.util.SupervisorUtil;

/**
 * @author trieu
 * 
 * the supervisor for all workers in running server (every server can run multiple workers with 1 supervisor only)
 *
 */
public class SupervisorNode  {

	static long uptime = 0;
	final static long period = 5000;
	
	static boolean started = false;
	static final long miliSecondsToSleep = 20000;
	static final Timer supervisorTimer = new Timer(true);
	
	public static void start(final boolean autoHeal){
		if(started){
			return;
		}
		supervisorTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				//LogUtil.i("...SupervisorNode autoHeal:"+autoHeal+" is sleeping in (milisecs)" + miliSecondsToSleep);
				SupervisorNode.checkWorkerStatus(autoHeal);
			}
		}, 2000, miliSecondsToSleep);
		started = true;
	}
	
	static {
		Timer timer = new Timer("uptime");
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
				uptime += period;
			}
		}, 0, period);
	}
	
	public static void checkWorkerStatus(){
		checkWorkerStatus(true);
	}
	

	public static void checkWorkerStatus(boolean autoHeal) {
		Map<String, WorkerInfo> workers = SupervisorUtil.getManagedWorkers();
		
		Set<String> workerNames = workers.keySet();
		List<String> healedWorkers = new ArrayList<>(workerNames.size());
		for (String workerName : workerNames) {
			WorkerInfo node = workers.get(workerName);
			try {				
				if(node != null){
					boolean isAlive = SupervisorUtil.ping(node);
					node.setAlive(isAlive);
					System.out.println("worker:"+workerName + " isAlive:"+isAlive + " autoHeal:" + autoHeal);
					ClusterDataManager.saveWorkerInfo(node);				
					if( !isAlive && autoHeal){
						healedWorkers.add(workerName);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		ClusterDataManager.asynchStartWorkers(healedWorkers);
	}

	public static String readableFileSize(long size) {
		if (size <= 0) {
			return "0";
		}
		final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
		int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size	/ Math.pow(1024, digitGroups))	+ " " + units[digitGroups];
	}

	
	public static void scheduleUpdateWorkerData(final WorkerNode workerNode,final String topic, final int beginPartitionId,	final int endPartitionId) {
		new Timer(true).schedule(new TimerTask() {
			@Override
			public void run() {
				SupervisorNode.updateWorkerData( workerNode, topic, beginPartitionId, endPartitionId);
			}
		}, 2000, period);
	}

	public static void updateWorkerData(WorkerNode workerNode, String topic, int beginPartitionId, int endPartitionId) {
		long uptimeHours = TimeUnit.MILLISECONDS.toHours(uptime);
		long uptimeMinutes = TimeUnit.MILLISECONDS.toMinutes(uptime);
		long uptimeSeconds = TimeUnit.MILLISECONDS.toSeconds(uptime);
		if (uptimeMinutes > 0) {
			uptimeSeconds = uptimeSeconds % (60 * uptimeMinutes);
		}
		if (uptimeHours > 0) {
			uptimeMinutes = uptimeMinutes % (60 * uptimeHours);
		}

		String formatedTime = StringUtil.toString(uptimeHours, ":",
				uptimeMinutes, ":", uptimeSeconds);
		
		String kafka_brokers = KafkaTopologyConfig.getBrokerList(topic).toString();

		Runtime rt = Runtime.getRuntime();
		String memoryUsed = readableFileSize(rt.totalMemory() - rt.freeMemory());
		String memoryLimit = readableFileSize(rt.maxMemory());
		
		String hostname = WorkerNode.getWorkerInfo().getHost();
		String workerName = WorkerNode.getWorkerName();
		String dataEmitterCount = "0";
		String topoName = "";
		String actorList = "";
		String totalActors = "0";
		if(workerNode instanceof KafkaProcessorNode){
			BaseTopology topology = ((KafkaProcessorNode)workerNode).getTopology();			
			if(topology != null){
				dataEmitterCount = topology.counter("DataEmitter").get() + "";
				topoName = topology.getClass().getSimpleName();
				actorList = getActorList(topology.getTopologyFlowsAsString());
				totalActors = topology.getTotalActors() + "";							
			}			
		} 
		WorkerData workerData = new WorkerData(memoryUsed, memoryLimit,
				workerName, hostname, topoName,
				topic, beginPartitionId + "-" + endPartitionId, kafka_brokers,
				dataEmitterCount, formatedTime,
				actorList,totalActors);
		ClusterDataManager.updateWorkerData(workerName, workerData);
	}

	static String getActorList(String actorList) {
		// System.out.println(actorList);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("<ol>");
		String[] array = actorList.split(" ");
		for (int i = 0; i < array.length; i++) {
			stringBuilder.append("<li>").append(array[i]).append("</li>");
		}
		stringBuilder.append("</ol>");
		return stringBuilder.toString();
	}


	public static void main(String[] args) {
		System.out.println("Starting SupervisorNode ...");		
		
		try {
			WorkerConfigs configs = WorkerConfigs.load();
			String path = configs.getStartWorkerScriptPath();

			Runtime.getRuntime().exec(path);
			Thread.sleep(2000);
			String ok = ""+ SupervisorUtil.isRemoteActorAlive("kafkaworkerserver1_127001_14003");
			System.out.println("ping:" + ok);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}