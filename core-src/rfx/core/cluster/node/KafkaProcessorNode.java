package rfx.core.cluster.node;

import java.util.List;

import rfx.core.cluster.ClusterDataManager;
import rfx.core.configs.KafkaTopologyConfig;
import rfx.core.exception.InvalidSetTaskDef;
import rfx.core.kafka.KafkaDataSeeder;
import rfx.core.model.KafkaTaskDef;
import rfx.core.model.TaskDef;
import rfx.core.topology.BaseTopology;
import rfx.core.util.LogUtil;
import rfx.core.util.Utils;
import rfx.core.util.WorkerUtil;

/**
 * the worker, that is dedicated for processing Kafka log only 
 * 
 * @author trieu
 *
 */
public class KafkaProcessorNode extends WorkerNode {
	private int timeToSeed = 1600;// Milliseconds	
	private BaseTopology topology;
	public static final String TYPENAME = "kafkaworker";
	
	public static KafkaProcessorNode getTheInstance() {
		return (KafkaProcessorNode) _instance;		
	}
	
	public BaseTopology getTopology() {
		return topology;
	}
	
	public KafkaTaskDef getTaskDef() {
		return (KafkaTaskDef) taskDef;
	}
	
	public KafkaProcessorNode() {
		super();
		initEmptyWorker(TYPENAME);
	}
	
	public KafkaProcessorNode(String workerName) {
		super();
		initWorkerByName(workerName);
	}
	
	public KafkaProcessorNode(BaseTopology topology) {
		super();
		initEmptyWorker(TYPENAME);
		this.topology = topology.buildTopology();		
	}
	
	public void startProcessingTask(int timeToSeed) {
		this.timeToSeed = timeToSeed;
		this.start();
	}
	
	@Override
	public boolean isReadyAutoStartProcessingTask(){
		TaskDef taskDef = ClusterDataManager.getTaskDefOfWorker(getWorkerName(), KafkaTaskDef.class);
		if(taskDef != null){
			try {
				setTaskDef(taskDef);
				return true;
			} catch (InvalidSetTaskDef e) {
				LogUtil.error(e);
			}			
		}
		return false;
	}
	
	@Override
	public void start() {		
		System.out.println("startComputation with timeToSeed:"+timeToSeed);
		if(topology != null){
			this.topology.scheduleEmittingData(timeToSeed);
		} else {			
			throw new IllegalStateException("startComputation failed! topology is NULL");
		}
	}
	
	@Override
	public void shutdownWorkerNode() {
		if(topology != null){
			topology.shutdownAll();
		}
	}

	@Override
	public void startupWorkerNode() {
		// TODO Auto-generated method stub		
	}
	
	@Override
	public void setTaskDef(TaskDef taskDef) throws InvalidSetTaskDef{
		//the task definition of worker
		if(taskDef instanceof KafkaTaskDef){
			if(this.taskDef == null){
				this.taskDef = taskDef;
				buildTopologyFromTaskDef();
			} else {
				throw new InvalidSetTaskDef("taskDef is assigned!");
			}
		} else {
			throw new InvalidSetTaskDef("taskDef must be instanceof KafkaTaskDef!");
		}
	}
	
	@Override
	public void resetTaskDef(TaskDef taskDef) throws InvalidSetTaskDef {
		//TODO shutdown and create new topology
		
	}

	@Override
	public void clearTaskDef() {
		// TODO Auto-generated method stub
		
	}
	
	void buildTopologyFromTaskDef(){
		String topic = getTaskDef().getTopic();
		int beginPartitionId = getTaskDef().getBeginPartitionId();
		int endPartitionId = getTaskDef().getEndPartitionId();
				
		// init mapDb Offset of kafka
		int offsetMapDbSize = KafkaDataSeeder.loadMapDbForTopic(topic , getWorkerName()).getOffsetMapDbSize();
		System.out.println("--------------offsetMapDbSize: " + offsetMapDbSize);
		
		// lookup the classpath for the topic
		String classpath = KafkaTopologyConfig.getTopologyClassPath(topic);
		if (classpath == null) {
			System.err.println("No mapping topology for topic:" + topic + " is NOT found in configs" );
		}
		System.out.println("For topic:" + topic + " , begin create topology: "	+ classpath);
		
		//build Kafka data seeders from parameters
		List<KafkaDataSeeder> dataSeeders = KafkaDataSeeder.buildKafkaDataSeeder(beginPartitionId, endPartitionId, topic);
		
		// inject params to topo
		this.topology = BaseTopology.lookupTopologyFromTopic(topic);
		
		if(this.topology != null){
			this.topology.setKafkaDataSeeders(dataSeeders).buildTopology();
			// start thread for monitoring worker
			SupervisorNode.scheduleUpdateWorkerData(this, topic,beginPartitionId, endPartitionId);			
			ClusterDataManager.updateTaskDefOfWorker(getWorkerName(), getTaskDef());			
			
			// start emit logs to the topology
			boolean autoStart = KafkaTopologyConfig.getKafkaConfigsForTopic(topic).isAutoStart();
			if (autoStart) {
				start();
			}
		}
	}
		

	////////////////////// Factory methods ////////////////////
	
	/**
	 * this's used for starting by supervisor
	 * 
	 */
	public static void startWorker(){
		// build the topology of actors
		WorkerNode workerNode = new KafkaProcessorNode();
		SupervisorNode.updateWorkerData(workerNode, "",0, 0);
	}
	
	/**
	 * this's used for starting by supervisor
	 * 
	 * @param workerName
	 */
	public static void startWorker(String workerName){
		// build the topology of actors
		WorkerNode workerNode = new KafkaProcessorNode(workerName);
		SupervisorNode.updateWorkerData(workerNode, "",0, 0);
	}

	/**
	 * this's used for debugging only
	 * 
	 * @param workerName
	 * @param topic
	 * @param beginPartitionId
	 * @param endPartitionId
	 */
	public static void startWorker(String workerName, String topic,	int beginPartitionId, int endPartitionId) {
		WorkerNode workerNode = initTopologyForWorker(workerName, topic, beginPartitionId, endPartitionId, true);		
		
		// start thread for monitoring worker
		SupervisorNode.scheduleUpdateWorkerData(workerNode, topic,beginPartitionId, endPartitionId);
	}

	/**
	 * this's used for debuging only
	 * 
	 * @param workerName
	 * @param topic
	 * @param beginPartitionId
	 * @param endPartitionId
	 * @param autoStart
	 */
	public static WorkerNode initTopologyForWorker(String workerName, String topic,	int beginPartitionId, int endPartitionId, boolean autoStart) {
		// init mapDb Offset of kafka
		int offsetMapDbSize = KafkaDataSeeder.loadMapDbForTopic(topic,workerName).getOffsetMapDbSize();
		System.out.println("--------------offsetMapDbSize: " + offsetMapDbSize);
		WorkerNode workerNode = null;
		try {
			// inject params to topo
			List<KafkaDataSeeder> dataSeeders = KafkaDataSeeder.buildKafkaDataSeeder(beginPartitionId, endPartitionId, topic);
			
			BaseTopology topology = BaseTopology.lookupTopologyFromTopic(topic);
			if(topology != null){
				topology.setKafkaDataSeeders(dataSeeders);			
	
				// build the topology of actors
				workerNode = new KafkaProcessorNode(topology);
				Utils.sleep(3000);
	
				// start emit logs to the topology
				if (autoStart) {
					workerNode.start();
				}				
			}
		} catch (Exception e) {
			LogUtil.error(e);
			WorkerUtil.autoSystemExit("ERROR: WorkerNode is shutdown, reason: "+e.getMessage(), 0);
		}
		return workerNode;
	}
	
}