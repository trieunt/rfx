package rfx.core.cluster.node;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import rfx.core.cluster.node.handler.MasterNodeResourceHandler;
import rfx.core.cluster.node.handler.MasterNodeRestHandler;

@ApplicationPath("/")
public class MasterNodeApp extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<Class<?>>();
        // register root resource
        classes.add(MasterNodeRestHandler.class);
        classes.add(MasterNodeResourceHandler.class);
        return classes;
    }
}
