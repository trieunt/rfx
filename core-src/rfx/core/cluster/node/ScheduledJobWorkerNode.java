package rfx.core.cluster.node;

import rfx.core.configs.ScheduledJobConfig;
import rfx.core.exception.InvalidSetTaskDef;
import rfx.core.job.ScheduledJobManager;
import rfx.core.model.TaskDef;
import rfx.core.util.LogUtil;


public class ScheduledJobWorkerNode extends WorkerNode {
	static final String TYPENAME = "ScheduledJob";
	public ScheduledJobWorkerNode() {
		super();
		initEmptyWorker(TYPENAME);
	}
	
	public static ScheduledJobWorkerNode getTheInstance() {
		return (ScheduledJobWorkerNode) _instance;		
	}

	@Override
	public void startupWorkerNode() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void shutdownWorkerNode() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean isReadyAutoStartProcessingTask(){
		return false;
	}

	@Override
	public void start() {
		int c = ScheduledJobManager.getInstance().startScheduledJobs(ScheduledJobConfig.DATA_SYNC_JOB);
		LogUtil.i("ScheduledJobWorkerNode", "startJobs:"+ c, true);		
		SupervisorNode.updateWorkerData(this, "",0, 0);
	} 
	
	@Override
	public void setTaskDef(TaskDef taskDef) throws InvalidSetTaskDef{
		// TODO Auto-generated method stub		
	}
	
	@Override
	public void resetTaskDef(TaskDef taskDef) throws InvalidSetTaskDef {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearTaskDef() {
		// TODO Auto-generated method stub
	}

	////////////////////// Factory methods ////////////////////
	
	public static void startWorker(){
		// build the topology of actors
		WorkerNode workerNode = new ScheduledJobWorkerNode();
		workerNode.start();
		
		// start thread for monitoring worker		
		SupervisorNode.updateWorkerData(workerNode, "",0, 0);
	}
	
}
