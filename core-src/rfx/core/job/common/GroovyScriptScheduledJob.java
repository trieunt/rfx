package rfx.core.job.common;

import rfx.core.job.ScheduledJob;
import rfx.core.util.GroovyScriptUtil;

public class GroovyScriptScheduledJob extends ScheduledJob {

	@Override
	public void runJob() {
		String out = GroovyScriptUtil.runAllInScheduledJobScripts();
		System.out.println(out);
	}

}
