package rfx.core.job.common;

import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.ClusterInfoConfigs;
import rfx.core.configs.RedisPoolConfigs;
import rfx.core.job.ScheduledJob;
import rfx.core.nosql.jedis.RedisCommand;

public class RealtimeDataPublisherJob extends ScheduledJob {
	
    public static final String CHANNEL_COMPACT_STATS = "compactStatsChannel";
    public static final String CHANNEL_FULL_STATS = "fullStatsChannel";
	
	static RedisPoolConfigs redisConfigs = RedisPoolConfigs.load();
	static ClusterInfoConfigs clusterInfoConfigs = ClusterInfoConfigs.load();
	static ShardedJedisPool pubsubPool = clusterInfoConfigs.getClusterInfoRedis().getShardedJedisPool();

	@Override
	public void runJob() {
		try {							
			(new RedisCommand<Void>(pubsubPool){
				@Override
				protected Void build() throws JedisException {
					//TODO
					return null;
				}				
			}).execute();
			
		} catch (Exception e) {					
			e.printStackTrace();			
		}
	}

}
