package rfx.core.job;

import java.util.Date;
import java.util.TimerTask;

import rfx.core.cluster.ClusterDataManager;
import rfx.core.configs.ScheduledJobConfig;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.LogUtil;


public abstract class ScheduledJob extends TimerTask{
	
	private ScheduledJobConfig scheduledJobsConfigs;
	protected int hourGoBack = 2;
	protected boolean isWorking;
	
	public ScheduledJob() {		
		super();		
	}
	
	public int getHourGoBack() {
		return hourGoBack;
	}

	public void setHourGoBack(int hourGoBack) {
		this.hourGoBack = hourGoBack;
	}

	public ScheduledJobConfig getScheduledJobsConfigs() throws IllegalArgumentException{
		if(scheduledJobsConfigs == null){
			throw new IllegalArgumentException("Missing scheduledJobsConfigs setting ");
		}
		return scheduledJobsConfigs;
	}

	public void setScheduledJobsConfigs(ScheduledJobConfig scheduledJobsConfigs) {
		this.scheduledJobsConfigs = scheduledJobsConfigs;
	}
		
	
	@Override
	public void run() {
		if (!isWorking) {
			isWorking = true;
			runJob();
			isWorking = false;
		} else {
			LogUtil.i("ScheduledJob is still working, skip !");
		}
	}
	
	protected void logJobResult(String jobName, int saveCount){
		String rsJob = DateTimeUtil.formatDateHourMinute(new Date()) +":"+saveCount;
		ClusterDataManager.logSynchDataJobResult(jobName + "-hourback:"+this.hourGoBack, rsJob);
	}
	
	/**
	 * the logic of job implementation
	 */
	public abstract void runJob();
	
}