package rfx.core.configs;

import java.util.ArrayList;
import java.util.List;

import rfx.core.configs.loader.Configurable;
import rfx.core.configs.loader.ParseConfigHandler;
import rfx.core.configs.loader.ParseXmlListObjectHandler;
import rfx.core.util.StringUtil;


public class ScheduledJobConfig implements Configurable {
	public final static int DATA_SYNC_JOB = 1;
	public final static int DATA_PUBLISHING_JOB = 2;
	
	private static List<ScheduledJobConfig> scheduledJobConfigs = new ArrayList<ScheduledJobConfig>();
	
	private String classpath;
	private long delay;
	private long period;
	private int jobType = DATA_SYNC_JOB;
	
	
	public ScheduledJobConfig() {
		
	}
	
	
	public  String getClasspath() {
		return classpath;
	}

	public void setClasspath(String classpath) {
		this.classpath = classpath;
	}

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}
	
	public long getPeriod() {
		return period;
	}

	public void setPeriod(long period) {
		this.period = period;
	}
	


	public int getJobType() {
		return jobType;
	}


	public void setJobType(int jobType) {
		this.jobType = jobType;
	}


	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("classpath:").append(classpath).append("-");
		s.append("delay:").append(delay).append("-");
		s.append("period:").append(period);		
		return s.toString();
	}

	
	public static List<ScheduledJobConfig> getConfigs(){		
		return scheduledJobConfigs;
	}


	@Override
	public ParseConfigHandler getParseConfigHandler() {		
		return new ParseXmlListObjectHandler() {			
			@Override
			public void injectFieldValue() {
				ScheduledJobConfig jobConfig = (ScheduledJobConfig)configurableObj;
				String classpath = xmlNode.attr("classpath");
				if(classpath != null){
					jobConfig.setClasspath(classpath);
					jobConfig.setDelay(StringUtil.safeParseLong(xmlNode.select("delay").text()));
					jobConfig.setPeriod(StringUtil.safeParseLong(xmlNode.select("period").text()));
					
					scheduledJobConfigs.add(jobConfig);
				}						
			}
		};
	}
}
