package rfx.system.starter;

import java.io.IOException;

import rfx.core.configs.ScheduledJobConfig;
import rfx.core.configs.loader.ConfigAutoLoader;
import rfx.core.job.ScheduledJobManager;
import rfx.core.util.LogUtil;
import rfx.core.util.Utils;

public class ScheduledJobNodeStarter {
	//ab -n 10000 -c 1000 -k -g j1K.tsv ""
	//ab -n 5000 -c 1000 -k -g test-5k-1K.tsv "http://localhost:8181/l?action_name=Tin%20t%E1%BB%A9c%20c%C3%B4ng%20ngh%E1%BB%87%20-%20Th%C3%B4ng%20tin%20th%E1%BA%BF%20gi%E1%BB%9Bi%20s%E1%BB%91%20internet%20-%20S%E1%BB%91%20H%C3%B3a%20VnExpress&h=14&m=40&s=16&url=http%3A%2F%2Fsohoa.vnexpress.net%2F&urlref=http%3A%2F%2Fvnexpress.net%2F&hostname=sohoa.vnexpress.net&beacon=zizgzlzlzrzkzmzrzlzqzozizmznzozmznzozizlzgzi2pzmzhzkzq2pzizgzlzrzlzfzgzrzr2pzmzizlzq2pzizgznzmzgzozizrzjzdziznzrzdziznzizdzhznzqzozmzj21zhzi1uzq1v201tzm1tzh1y1vzg&res=1360x768&_id=a95bc743063ad1ba"
		
	public static void main(String[] args) throws IOException {	
		LogUtil.i("--JVM: " + System.getProperty("sun.arch.data.model") + " bit, version: " + System.getProperty("java.version"));
		try {			
			//ready for get tasks from master
			ConfigAutoLoader.loadAll();
			//ScheduledJobWorkerNode.startWorker();
			int c = ScheduledJobManager.getInstance().startScheduledJobs(ScheduledJobConfig.DATA_SYNC_JOB);
			LogUtil.i("ScheduledJobWorkerNode", "startJobs:"+ c, true);	
			while (c>0) {
				Utils.sleep(1000);
			}
		} catch (Exception e) {			
			e.printStackTrace();
			System.exit(1);
		}
	}
	
}
