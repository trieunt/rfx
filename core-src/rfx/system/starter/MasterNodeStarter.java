package rfx.system.starter;

import rfx.core.cluster.node.MasterNode;
import rfx.core.configs.ClusterInfoConfigs;
import rfx.core.configs.loader.ConfigAutoLoader;

public class MasterNodeStarter {	
	
	public static void main(String[] args) {
		ConfigAutoLoader.loadAll();
		ClusterInfoConfigs configs = ClusterInfoConfigs.load();
		new MasterNode(configs.getMasterHostname(), configs.getMasterHttpPort(),configs.getMasterWebSocketPort()).run();
	}
}